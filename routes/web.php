<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\CategoryMenu;
use App\ContactMessage;
use App\Http\Requests\ContactMessageRequest;
use function App\lib\test;
use function App\lib\wp_print;
use App\Menu;
use App\Order;
use App\TableOrder;
use Illuminate\Support\Facades\Route;
use Mailgun\Mailgun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


Route::auth();



Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/loso',function(){

  return view('loso.Loso-responsive-layout-demonstration.index');

});



//Route::get('/shop/wechatQrcode',function(){
//
//    return view('functional.qr');
//
//});
//
//Route::get('/shop/qrcode',function(){
//
//    return view('functional.qrcode');
//
//});

Route::post('/shop/cart/add', 'CartController@addToCart')->name('shop.cart.add');

Route::post('/shop/cart/minus', 'CartController@minusFromCart')->name('shop.cart.minus');

Route::post('/shop/cart/updateChoice', 'CartController@updateChoice')->name('shop.cart.updateChoice');

Route::get('/shop/cart/orderAddress', 'CartController@orderAddress')->name('shop.cart.orderAddress');

Route::post('/shop/cart/goToCheckout', 'CartController@goToCheckout')->name('shop.cart.goToCheckout');

Route::post('/shop/cart/checkout', 'CartController@checkout')->name('shop.cart.checkout');

Route::get('/shop/cart/wechatCheckout', 'CartController@wechatCheckout')->name('shop.cart.wechatCheckout');

Route::get('/shop/cart/aliCheckout', 'CartController@aliCheckout')->name('shop.cart.aliCheckout');

Route::get('/shop/cart/wechatPayCallBack', 'CartController@payCallBack')->name('shop.cart.payCallBack');

Route::get('/shop/cart/receipt/{receipt_no}', 'CartController@receipt')->name('shop.cart.receipt');

Route::resource('/shop/menu', 'MenuController',['names'=>[

    'index'=>'shop.menu.index',

]]);

Route::post('/subscribe', function (Request $request){

    $email = $request->email_newsletter_2;

    $mgClient = new Mailgun('key-fd15dc1b7e65593fe14af87005b61b52');
    $listAddress = 'subscriber@mg.bruceproject.review';

# Issue the call to the client.
    $result = $mgClient->post("lists/$listAddress/members", array(
        'address'     => $email,
        'subscribed'  => true,
    ));

    $mgClient->sendMessage('mg.bruceproject.review',[
        'from' =>'programerbruce@gmail.com',
        'to' => $email,
        'subject' => 'You have subscribed our newletter service',
        'html' => "Dear Sir/Madam:<p>Thanks for your confirming, you are now subscribed our newsletter service.</p>" .
            "Thanks again. And hoping you could have wonderful experience with us." .
            "<br><br>Kind regards,<br>Quickfood team"
    ]);
    return redirect()->back()->with('subscribe_status','You have subscribed Newsletter successfully.');


})->name('subscribe');

Route::get('/goToUnsubscribe', function (){

    return view('functional.unsubscribe');

})->name('goToUnsubscribe');

Route::get('/goToTracker', function (){

    return view('functional.tracker');

})->name('goToTracker');

Route::post('/ajaxRenderTracker', function (Request $request){

    $order_no = $request->order_no;
    $orders = Order::where('payment_id', $order_no)->orderBy('updated_at', 'asc')->get();
    foreach ($orders as $order) {
        $order->cart = unserialize($order->cart);
    }


    return response()->json($orders);
})->name('ajaxRenderTracker');

Route::get('/contact', function (){

    return view('shop.contact.index');

})->name('contact');

Route::post('/contact/save', function (ContactMessageRequest $request){
    $name = $request->name ;
    $email = $request->email;
    $phone = $request->phone;
    $message = $request->message;

    $request['status'] = 1;

    ContactMessage::create($request->all());

    return redirect(route('contact'))->with('contact_save_status','Thanks for getting in touch with us. We will reply your message soon.');


})->name('saveContact');

Route::post('/unsubscribe', function (Request $request){
    $email = $request->email_newsletter_2;
    # Instantiate the client.
    $mgClient = new Mailgun('key-fd15dc1b7e65593fe14af87005b61b52');
    $listAddress = 'subscriber@mg.bruceproject.review';
    $listMember = $email;

# Issue the call to the client.
    $result = $mgClient->delete("lists/$listAddress/members/$listMember");

    $mgClient->sendMessage('mg.bruceproject.review',[
        'from' =>'programerbruce@gmail.com',
        'to' => $email,
        'subject' => 'You have unsubscribe our newletter service',
        'html' => "Dear Sir/Madam:<p>You have unsubscribe our newletter service.</p>" .
            "You can subscribe our page again on our site once you want it." .
            "<br><br>Kind regards,<br>Quickfood team"
    ]);

    return redirect(route('goToUnsubscribe'))->with('unsubscribe_info','You have unsubscribe Newsletter successfully.');

})->name('unsubscribe');

Route::resource('/blog', 'BlogController',['names'=>[

    'index'=>'blog.index',
    'create'=>'blog.create',
    'store'=>'blog.store',
    'edit'=>'blog.edit',
    'show'=>'blog.post',
]]);


Route::resource('/blog/comment', 'CommentController',['names'=>[

    'index'=>'blog.comment.index',

]]);



Route::middleware(['user'])->group(function () {

    Route::resource('/blog/comment-reply', 'CommentReplyController',['names'=>[


    ]]);

    Route::resource('/user', 'UserController',['names'=>[

        'index'=>'shop.user.index',

    ]]);

    Route::post('/user/updatePhone', 'UserController@updatePhone')->name('shop.user.updatePhone');

    Route::post('/user/updateName', 'UserController@updateName')->name('shop.user.updateName');

    Route::post('/user/updatePhoto', 'UserController@updatePhoto')->name('shop.user.updatePhoto');

//    Route::post('/user/changePassword', 'UserController@changePassword')->name('shop.user.changePassword');

    Route::get('/user/orders/{user_id}', 'UserController@orders')->name('shop.user.orders');





});


Route::middleware(['admin'])->group(function () {

    Route::resource('/admin/menu/category', 'AdminMenuCategoryController',['names'=>[

        'index'=>'admin.menu.category.index',
        'create'=>'admin.menu.category.create',
        'edit'=>'admin.menu.category.edit',

    ]]);


    Route::post('/admin/menu/photo/edit', 'AdminMenuController@photo')->name('admin.menu.photo.edit');





    Route::resource('/admin/menu', 'AdminMenuController',['names'=>[

        'index'=>'admin.menu.index',
        'create'=>'admin.menu.create',
        'edit'=>'admin.menu.edit',

    ]]);



    Route::resource('/admin/user', 'AdminUserController',['names'=>[

        'index'=>'admin.user.index',

    ]]);

    Route::resource('/admin/blog/comments', 'AdminBlogCommentsController',['names'=>[

        'index'=>'admin.blog.comments.index',

    ]]);


    Route::get('/admin', function () {

        $no_messages = count(ContactMessage::where('status',1)->get());

        return view('admin.dashboard',compact('no_messages'));
    })->name('admin.dashboard');

    Route::resource('/admin/contactMessage', 'AdminContactMessageController',['names'=>[

        'index'=>'admin.contactMessage.index',

    ]]);


    Route::resource('/admin/blog', 'AdminBlogController',['names'=>[

        'index'=>'admin.blog.index',
        'edit' => 'admin.blog.edit',
        'create' => 'admin.blog.create',
        'destroy' => 'admin.blog.destroy',

    ]]);

    Route::post('/admin/blog/updatePhoto', 'AdminBlogController@updatePhoto')->name('admin.blog.updatePhoto');

    Route::post('/admin/blog/addCategory', 'AdminBlogController@addCategory')->name('admin.blog.addCategory');

    Route::get('/admin/order', 'AdminOrderController@index')->name('admin.order.index');

    Route::post('/admin/order/ajaxRenderOrder', 'AdminOrderController@ajaxRenderOrder')->name('admin.order.ajaxRenderOrder');

    Route::post('/admin/order/ajaxUpdateOrderToKitchen', 'AdminOrderController@ajaxUpdateOrderToKitchen')->name('admin.order.ajaxUpdateOrderToKitchen');

    Route::get('/admin/order/kitchen', 'AdminOrderController@goToKitchen')->name('admin.order.kitchen');

    Route::post('/admin/order/kitchen/ajaxRenderKitchenOrder', 'AdminOrderController@ajaxRenderKitchenOrder')->name('admin.order.ajaxRenderKitchenOrder');

    Route::post('/admin/order/kitchen/ajaxUpdateOrderToDelivery', 'AdminOrderController@ajaxUpdateOrderToDelivery')->name('admin.order.ajaxUpdateOrderToDelivery');

    Route::get('/admin/order/management', 'AdminOrderController@orderManagement')->name('admin.order.management');

    Route::post('/admin/order/management/update', 'AdminOrderController@orderManagementUpdate')->name('admin.order.management.update');

    Route::get('/admin/order/goToDelivery', 'AdminOrderController@goToDelivery')->name('admin.order.goToDelivery');

    Route::post('/admin/order/goToDelivery/ajaxRenderDeliveryOrder', 'AdminOrderController@ajaxRenderDeliveryOrder')->name('admin.order.goToDelivery.ajaxRenderDeliveryOrder');

    Route::post('/admin/order/goToDelivery/ajaxUpdateOrderToFinish', 'AdminOrderController@ajaxUpdateOrderToFinish')->name('admin.order.goToDelivery.ajaxUpdateOrderToFinish');

});




Route::get('/tableOrder/{tableNo}',function($tableNo){
    $categories = CategoryMenu::all();
    $menus = Menu::all();
    $data = array('tableNo' => $tableNo,'categories' => $categories,'menus' => $menus);
    return view('shop.mobile.tableOrder',compact('data'));

})->name('tableOrder');

Route::post('/ajaxRenderMenus',function(Request $request){
    $category_menus_id = \App\CategoryMenu::where('name',$request->get('categoryName'))->first()->id;
    $menus = \App\Menu::where('category_menus_id',$category_menus_id)->get();
    return response()->json($menus);

})->name('ajaxRenderMenus');

Route::get('/ajaxGetMenu/{menuId}',function($menuId){
    $menu = \App\Menu::findOrFail($menuId);
    return response()->json($menu);

})->name('ajaxGetMenu');

Route::post('/ajaxAddMenuToTable',function(Request $request){
    $tableNo = $request->get('tableNo');
    $menu_id = $request->get('menuId');
    $qty = $request->get('qty');

    $menu = \App\TableOrder::where('tableNo',$tableNo)->where('menu_id',$menu_id)->where('status',1)->first();

    if($menu){
        $menu->qty = $menu->qty + $qty;
        $menu->save();
    }else{
        $tableOrder = new TableOrder();
        $tableOrder->tableNo = $tableNo;
        $tableOrder->menu_id = $menu_id;
        $tableOrder->qty = $qty;
        $tableOrder->save();
    }

    return response()->json('{"success": true}');

})->name('ajaxGetMenu');


Route::get('/tableOrderReview/{tableNo}',function($tableNo){

    $menus = TableOrder::where('tableNo',$tableNo)->where('status',1)->get();

    $printed_menus = TableOrder::where('tableNo',$tableNo)->where('status',2)->get();
    $data = ['tableNo' => $tableNo, 'menus' => $menus, 'printed_menus' => $printed_menus];

    return view('shop.mobile.tableOrderReview',compact('data'));

})->name('tableOrderReview');

Route::post('/tableOrderSendToKitchen',function(Request $request){
    $tableNo = $request->get('tableNo');
    $ordered_menus = TableOrder::where('tableNo',$tableNo)->where('status',1)->get();
    $arr = array();
    foreach ($ordered_menus as $tableOrder){
        $tableOrder->status = 2;
        $tableOrder->save();



        $orderItem= array('title'=>$tableOrder->menu->name,'price'=>$tableOrder->menu->price,'num'=>$tableOrder->qty);
        array_push($arr,$orderItem);
    }
    $orderInfo = test($arr,14,6,3,6,'桌号' . $tableNo,'店内点单','无号码','没备注','','','2');//名称14 单价6 数量3 金额6-->这里的字节数可按自己需求自由改写
    wp_print('718500705',$orderInfo,1);

    return redirect()->route('tableOrderReview',['tableNo' => $tableNo]);

})->name('tableOrderSendToKitchen');

Route::get('/selectTable',function(){


    return view('shop.mobile.selectTable');

})->name('selectTable');

Route::post('/selectTableRedirect',function(Request $request){
    $tableNo = $request->get('tableNo');

    return redirect()->route('tableOrder',['tableNo' => $tableNo]);

})->name('selectTableRedirect');

Route::post('/deleteOrderedItemOnce',function(Request $request){
    $tableNo = $request->get('tableNo');
    $tableOrderId = $request->get('tableOrderId');

    $ordered_menu = TableOrder::findOrFail($tableOrderId);

    if($ordered_menu->qty == 1){
        $ordered_menu->delete();
    }elseif($ordered_menu->qty > 1){
        $ordered_menu->qty = $ordered_menu->qty - 1;
        $ordered_menu->save();
    }

    return redirect()->route('tableOrderReview',['tableNo' => $tableNo]);

})->name('deleteOrderedItemOnce');

Route::post('/deleteOrderedItemAll',function(Request $request){
    $tableNo = $request->get('tableNo');
    $tableOrderId = $request->get('tableOrderId');

    $ordered_menu = TableOrder::findOrFail($tableOrderId);

    $ordered_menu->delete();

    return redirect()->route('tableOrderReview',['tableNo' => $tableNo]);

})->name('deleteOrderedItemAll');


