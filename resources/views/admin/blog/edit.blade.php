@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="padding: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">Edit post</li>
        </ol>
        {!! Form::open(['method'=>'POST','action'=>'AdminBlogController@updatePhoto', 'files' => true]) !!}
        <input type="hidden" name="post_id" value="{{$post->id}}">
            <div class="form-group">
                <img src="{{asset($post->photos()->first()->file)}}" alt="">
                {!! Form::file('photo', null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Update Photo', ['class'=>'btn btn-primary']) !!}
            </div>
        {!! Form::close() !!}


        {!! Form::open(['method'=>'PATCH','action'=>['AdminBlogController@update',$post->id], 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('title','Title') !!}
                    {!! Form::text('title',$post->title,['class'=>'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('body','Body') !!}
                    @include('includes.editor')
                    <textarea name="body" class="form-control my-editor" rows="20">{!! old('body', $post->body) !!}</textarea>
                </div>

                <div class="form-group">
                    {!! Form::submit('Edit Post', ['class'=>'btn btn-primary','rows'=>3]) !!}
                </div>
            {!! Form::close() !!}

    </div>

@stop