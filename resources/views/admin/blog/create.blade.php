@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">Edit post</li>
        </ol>

        @include('includes.form_error')

        {!! Form::open(['method'=>'POST','action'=>'AdminBlogController@addCategory']) !!}
        <div class="form-group">
            {!! Form::label('new_category_name','Add Category') !!}
            {!! Form::text('new_category_name',null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Add Category', ['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}

        {!! Form::open(['method'=>'POST','action'=>'AdminBlogController@store', 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('title','Title') !!}
                    {!! Form::text('title',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('category_id','Category') !!}
                    {!! Form::select('category_id', [''=>'Choose Category'] + $categories, null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('photo','Post image') !!}
                    {!! Form::file('photo', null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('body','Body') !!}
                    @include('includes.editor')
                    <textarea name="body" class="form-control my-editor"></textarea>
                </div>

                <div class="form-group">
                    {!! Form::submit('Create Post', ['class'=>'btn btn-primary','rows'=>3]) !!}
                </div>
            {!! Form::close() !!}

    </div>

@stop


