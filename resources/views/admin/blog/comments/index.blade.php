@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">All posts</li>
        </ol>
        <table class="table">
            <thead>
            <tr>
                <th>id</th>
                <th>body</th>
                <th>created time</th>
                <th>updated time</th>
                <th>author</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            @if(count($comments) > 0)
                @foreach($comments as $comment)
                    <tbody>
                    <tr>
                        <td>{{$comment->id}}</td>
                        <td><textarea cols="25" rows="1">{{$comment->body}}</textarea></td>
                        <td>{{$comment->created_at->diffForHumans()}}</td>
                        <td>{{$comment->updated_at->diffForHumans()}}</td>
                        <td>{{$comment->user->name}}</td>
                        <td>
                            @if($comment->is_active == 1)
                                {!! Form::open(['method'=>'PATCH','action'=>['AdminBlogCommentsController@update', $comment->id]]) !!}

                                <input type="hidden" name="is_active" value="0">

                                <div class="form-group">
                                    {!! Form::submit('Un-approve', ['class'=>'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['method'=>'PATCH','action'=>['AdminBlogCommentsController@update', $comment->id]]) !!}

                                <input type="hidden" name="is_active" value="1">

                                <div class="form-group">
                                    {!! Form::submit('Approve', ['class'=>'btn btn-info']) !!}
                                </div>
                                {!! Form::close() !!}
                            @endif
                        </td>
                        <td>
                            {!! Form::open(['method'=>'DELETE','action'=>['AdminBlogCommentsController@destroy', $comment->id]]) !!}

                            <div class="form-group">
                                {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                            </div>

                            {!! Form::close() !!}
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            @endif
        </table>
    </div>

@stop