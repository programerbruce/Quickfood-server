@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">All posts</li>
        </ol>
        <table class="table table-responsive">
            <thead>
              <tr>
                <th>id</th>
                <th>title</th>
                <th>created time</th>
                <th>updated time</th>
                <th>owner</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            @if(count($posts) > 0)
            @foreach($posts as $post)
            <tbody>
              <tr>
                <td>{{$post->id}}</td>
                <td><a href="{{route('blog.post', $post->id)}}">{{$post->title}}</a></td>
                <td>{{$post->created_at->diffForHumans()}}</td>
                <td>{{$post->updated_at->diffForHumans()}}</td>
                <td>{{$post->user->name}}</td>
                <td>
                    <a href="{{route('admin.blog.edit', $post->id)}}">
                        <button class="btn btn-primary">Edit</button>
                    </a>
                </td>
                <td>
                     {!! Form::open(['method'=>'DELETE','action'=> ['AdminBlogController@destroy',$post->id]]) !!}
                            <div class="form-group">
                                {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                            </div>
                      {!! Form::close() !!}
                </td>
              </tr>
            </tbody>
            @endforeach
            @endif
          </table>
    </div>

@stop
