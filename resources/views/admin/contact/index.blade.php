@extends('admin.layouts.admin')

@section('css')
    <!-- BASE CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
@stop




@section('content')
    <div style="margin: 20px" class="container-fluid">
        @if(count($messages) == 0)
            <div style="margin: 20px" class="alert alert-primary" role="alert">
                Currently,you have no incoming contact messages!
            </div>
        @endif
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">All contact message</li>
        </ol>
    </div>

    <div style="margin: 20px" class="row">
        @if(count($messages) > 0)
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>phone</th>
                    <th>email</th>
                    <th>message</th>
                    <th>created time</th>
                    <th>updated time</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
            @foreach($messages as $message)
                <tbody>
                <tr>
                    <td>{{$message->id}}</td>
                    <td>{{$message->name}}</td>
                    <td>{{$message->phone}}</td>
                    <td>{{$message->email}}</td>
                    <td><textarea cols="25" rows="1">{{$message->message}}</textarea></td>
                    <td>{{$message->created_at->diffForHumans()}}</td>
                    <td>{{$message->updated_at->diffForHumans()}}</td>
                    <td>
                        @if($message->status == 1)
                            {!! Form::open(['method'=>'PATCH','action'=>['AdminContactMessageController@update', $message->id]]) !!}

                            <input type="hidden" name="is_resolve" value="2">

                            <div class="form-group">
                                {!! Form::submit('Un-resolved', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['method'=>'PATCH','action'=>['AdminContactMessageController@update', $message->id]]) !!}

                            <input type="hidden" name="is_resolve" value="1">

                            <div class="form-group">
                                {!! Form::submit('Resolved', ['class'=>'btn btn-info']) !!}
                            </div>
                            {!! Form::close() !!}
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE','action'=>['AdminContactMessageController@destroy', $message->id]]) !!}

                        <div class="form-group">
                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                        </div>

                        {!! Form::close() !!}
                    </td>
                </tr>
                </tbody>
            @endforeach
            @endif
    </table>
            <div class="centered">{{$messages->links()}}</div>
    </div>





@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop
