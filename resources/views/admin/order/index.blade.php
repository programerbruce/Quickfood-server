@extends('admin.layouts.admin')

@section('css')
    <!-- BASE CSS -->
    <link href="{{asset('css/base.css')}}" rel="stylesheet">
    <link href="{{asset('tableorder.css')}}" rel="stylesheet">
@stop

@section('content')
    <!-- Content ================================================== -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">Received Orders</li>
        </ol>

         {{--<!-- UI X -->--}}
            <div id="order_container" style="margin: 0px">

             </div><!-- End container -->
    </div>
    <!-- End Content =============================================== -->
@stop

@section('scripts')

    <script>
        $(document).on('click','#order_process_btn',function(){

            var radioVal = $("input[name='option_2']:checked").val();

            alert('Clicked hehe:' + radioVal);
            $('#service_type').val(radioVal);
            alert('service_type:' + $('#service_type').val());
            $( "#cart_form" ).submit();
        });
    </script>

    <script>
        setInterval(ajaxCall, 3000); //300000 MS == 5 minutes

        function ajaxCall() {
            renderPage();
        }
        $(document).ready(function(){
            renderPage();
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });

        function renderPage() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // alert('Menu id' + menuId);
            $.ajax({
                type: 'POST',
                url: 'order/ajaxRenderOrder',
                data: '',
                dataType: "json",
                success: function (data) {
                    var orders = data;

                    console.log(data);
                    var html = '';

                    for (i = 0; i < orders.length; i += 3) {

                        var order_one = null;
                        var order_two = null;
                        var order_three = null;

                        if (orders[i]) {
                            order_one = orders[i];
                        }

                        if ((i + 1) < orders.length) {
                            order_two = orders[i + 1];
                        }

                        if ((i + 2) < orders.length) {
                            order_three = orders[i + 2];
                        }
                        html += '<div class="row">';
                        if (order_one != null) {
                            html += '<div class="col-md-4">' +
                                '<div class="box_style_2">' +
                                '<h2 class="inner">Order Received</h2>' +
                                '<h2 class="inner">' + order_one.created_at +'</h2>' +
                                '<div id="confirm">' +
                                '<div>' +
                                '<h4>Order no: ' +  order_one.id + '</h4>' +
                                '<h4>Customer: ' + order_one.name + '</h4>' +
                                '<h4>Address: ' + order_one.address + '</h4>' +
                                '<h4>Phone: ' + order_one.cart.addressDetail['phone'] + '</h4>' +
                                '<br>' +
                                '<h4>Desire Time: ' + order_one.desire_time + '</h4>' +
                                '</div>' +
                                '</div>' +
                                '<h4>Summary</h4>' +
                                '<table class="table table-striped nomargin">' +
                                '<thead>' +
                                '<tr>' +
                                '<th>Dish</th>' +
                                '<th>QTY</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>';
                            $.each(order_one.cart.items, function (index, item) {
                                html += '<tr>' +
                                    '<td>' +
                                    '<strong>' + item['item']['name'] +
                                    '</td>' +
                                    '<td>' +
                                    '<strong class="pull-right">' + item['qty'] + '</strong>' +
                                    '</td>' +
                                    '</tr>'
                            });
                            html += '</tbody>' +
                                '</table>';
                            if(order_one.cart.addressDetail['Note'] != null){
                                html += '<h4><font color="red">Note:</font></h4>'+
                                '<p>' + order_one.cart.addressDetail['Note'] + '</p>';
                            }

                            html += '<form action="{{route("admin.order.ajaxUpdateOrderToKitchen")}}" method="post">' +
                                    '<input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />' +
                                    '<input type="hidden" name="order_id" value="'+ order_one.id + '"/>' +
                                '<button style="margin: 10px;" class="btn btn-primary">Send to Kitchen</button>' +
                            '</form>';

                                html += '</div></div>';
                        }

                        if (order_two != null) {
                            html += '<div class="col-md-4">' +
                                '<div class="box_style_2">' +
                                '<h2 class="inner">Order Received</h2>' +
                                '<h2 class="inner">' + order_two.created_at +'</h2>' +
                                '<div id="confirm">' +
                                '<h4>Order no: ' +  order_two.id + '</h4>' +
                                '<h4>Customer: ' + order_two.name + '</h4>' +
                                '<h4>Address: ' + order_two.address + '</h4>' +
                                '<h4>Phone: ' + order_two.cart.addressDetail['phone'] + '</h4>' +
                                '<br>' +
                                '<h4>Desire Time: ' + order_two.desire_time + '</h4>' +
                                '</div>' +
                                '<h4>Summary</h4>' +
                                '<table class="table table-striped nomargin">' +
                                '<thead>' +
                                '<tr>' +
                                '<th>Dish</th>' +
                                '<th>QTY</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>';
                            $.each(order_two.cart.items, function (index, item) {
                                html += '<tr>' +
                                    '<td>' +
                                    '<strong>' + item['item']['name'] +
                                    '</td>' +
                                    '<td>' +
                                    '<strong class="pull-right">' + item['qty'] + '</strong>' +
                                    '</td>' +
                                    '</tr>'
                            });
                            html += '</tbody>' +
                                '</table>';
                            if(order_two.cart.addressDetail['Note'] != null){
                                html += '<h4><font color="red">Note:</font></h4>'+
                                    '<p>' + order_two.cart.addressDetail['Note'] + '</p>';
                            }
                            html += '<form action="{{route("admin.order.ajaxUpdateOrderToKitchen")}}" method="post">' +
                                '<input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />' +
                                '<input type="hidden" name="order_id" value="'+ order_two.id + '"/>' +
                                '<button style="margin: 10px;" class="btn btn-primary">Send to Kitchen</button>' +
                                '</form>';

                            html += '</div></div>';
                        }


                        if (order_three != null) {
                            html += '<div class="col-md-4">' +
                                '<div class="box_style_2">' +
                                '<h2 class="inner">Order Received</h2>' +
                                '<h2 class="inner">' + order_three.created_at +'</h2>' +
                                '<div id="confirm">' +
                                '<h4>Order no: ' +  order_three.id + '</h4>' +
                                '<h4>Customer: ' + order_three.name + '</h4>' +
                                '<h4>Address: ' + order_three.address + '</h4>' +
                                '<h4>Phone: ' + order_three.cart.addressDetail['phone'] + '</h4>' +
                                '<br>' +
                                '<h4>Desire Time: ' + order_three.desire_time + '</h4>' +
                                '</div>' +
                                '<h4>Summary</h4>' +
                                '<table class="table table-striped nomargin">' +
                                '<thead>' +
                                '<tr>' +
                                '<th>Dish</th>' +
                                '<th>QTY</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>';
                            $.each(order_three.cart.items, function (index, item) {
                                html += '<tr>' +
                                    '<td>' +
                                    '<strong>' + item['item']['name'] +
                                    '</td>' +
                                    '<td>' +
                                    '<strong class="pull-right">' + item['qty'] + '</strong>' +
                                    '</td>' +
                                    '</tr>'
                            });
                            html += '</tbody>' +
                                '</table>';
                            if(order_three.cart.addressDetail['Note'] != null){
                                html += '<h4><font color="red">Note:</font></h4>'+
                                    '<p>' + order_three.cart.addressDetail['Note'] + '</p>';
                            }
                            html += '<form action="{{route("admin.order.ajaxUpdateOrderToKitchen")}}" method="post">' +
                                '<input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />' +
                                '<input type="hidden" name="order_id" value="'+ order_three.id + '"/>' +
                                '<button style="margin: 10px;" class="btn btn-primary">Send to Kitchen</button>' +
                                '</form>';

                            html += '</div></div>';
                        }


                        html += '</div>';
                    }

                    if(orders.length == 0){
                        html += '<p class="text-center">Recently, no incoming orders received.</p>';
                    }

                    $('#order_container').html(html);

                }
            });
        }



    </script>




@stop
