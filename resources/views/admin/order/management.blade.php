@extends('admin.layouts.admin')




@section('content')
    <!-- Content ================================================== -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">Order Management</li>
        </ol>

        {{--<!-- UI X -->--}}
        <div id="order_container" style="margin: 0px">

            @if(count($orders) > 0)

            @for ($i = 0; $i < count($orders); $i += 3)

            @php ($order_one = null)
            @php ($order_two = null)
            @php ($order_three = null)

             @if ($orders[$i])
             @php ($order_one = $orders[$i])
             @endif

             @if(($i + 1) < count($orders))
             @php($order_two = $orders[$i + 1])
             @endif

             @if(($i + 2) < count($orders))
             @php($order_three = $orders[$i + 2])
             @endif
        <div class="row">
         @if($order_one != null)
            <div class="col-md-4">
               <div class="box_style_2">
                     <h2 class="inner">Order</h2>
                     <h2 class="inner">{{$order_one->created_at}}</h2>
                     <div id="confirm">
                         <div>
                             <h4>PaymentId:  {{$order_one->payment_id}}</h4>
                             <h4>Order no:  {{$order_one->id}}</h4>
                             <h4>Customer:  {{$order_one->name}}</h4>
                             <h4>Address:  {{$order_one->address}}</h4>
                             <h4>Phone: {{$order_one->cart->addressDetail['phone']}}</h4>
                             <br>
                             <h4>Desire Time: {{$order_one->desire_time}}</h4>
                             </div>
                         </div>
                     <h4>Summary</h4>
                     <table class="table table-striped nomargin">
                         <thead>
                         <tr>
                             <th>Dish</th>
                             <th>QTY</th>
                             </tr>
                         </thead>
                         <tbody>
                        @foreach($order_one->cart->items as $item)
                         <tr>
                             <td>
                                 <strong>{{$item['item']['name']}}</strong>
                             </td>
                             <td>
                                 <strong class="pull-right">{{$item['qty']}} </strong>
                             </td>
                             </tr>
                        @endforeach
                         </tbody>
                         </table>
                     @if($order_one->cart->addressDetail['Note'] != null)
                     <h4><font color="red">Note:</font></h4>
                     <p>{{$order_one->cart->addressDetail['Note']}}</p>
                     @endif
                   <form id="update_form1" style="margin-top: 20px" action="{{route("admin.order.management.update")}}" method="post">
                       <table class="table">
                           <tbody>
                           <tr>
                               <td>
                                   <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                                   <input type="hidden" name="order_id" value="{{$order_one->id}}">
                                   <div class="form-group">
                                       {!! Form::select('status', array('' => 'Select status', '1' => 'Order received','2' => 'Order in kitchen','3' => 'Order on delivery','4' => 'Order Finalised'),$order_one->status, ['class'=>'form-control']) !!}
                                   </div>
                               </td>
                               <td>
                                   <button class="btn btn-primary">Update</button>
                               </td>
                           </tr>
                           </tbody>
                       </table>
                   </form>

                     </div>
           </div>
         @endif
             @if($order_two != null)
                 <div class="col-md-4">
                     <div class="box_style_2">
                         <h2 class="inner">Order</h2>
                         <h2 class="inner">{{$order_two->created_at}}</h2>
                         <div id="confirm">
                                 <div>
                                 <h4>Order no:  {{$order_two->id}}</h4>
                                 <h4>Customer:  {{$order_two->name}}</h4>
                                 <h4>Address:  {{$order_two->address}}</h4>
                                 <h4>Phone: {{$order_two->cart->addressDetail['phone']}}</h4>
                                 <br>
                                 <h4>Desire Time: {{$order_two->desire_time}}</h4>
                             </div>
                         </div>
                         <h4>Summary</h4>
                         <table class="table table-striped nomargin">
                             <thead>
                             <tr>
                                 <th>Dish</th>
                                 <th>QTY</th>
                             </tr>
                             </thead>
                             <tbody>
                             @foreach($order_two->cart->items as $item)
                                 <tr>
                                     <td>
                                         <strong>{{$item['item']['name']}}</strong>
                                     </td>
                                     <td>
                                         <strong class="pull-right">{{$item['qty']}} </strong>
                                     </td>
                                 </tr>
                             @endforeach
                             </tbody>
                         </table>
                         @if($order_two->cart->addressDetail['Note'] != null)
                             <h4><font color="red">Note:</font></h4>
                             <p>{{$order_two->cart->addressDetail['Note']}}</p>
                         @endif
                         <form id="update_form2" style="margin-top: 20px" action="{{route("admin.order.management.update")}}" method="post">
                             <table class="table">
                                 <tbody>
                                 <tr>
                                     <td>
                                         <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                                         <input type="hidden" name="order_id" value="{{$order_two->id}}">
                                         <div class="form-group">
                                             {!! Form::select('status', array('' => 'Select status', '1' => 'Order received','2' => 'Order in kitchen','3' => 'Order on delivery','4' => 'Order Finalised'),$order_two->status, ['class'=>'form-control']) !!}
                                         </div>
                                     </td>
                                     <td>
                                         <button class="btn btn-primary">Update</button>
                                     </td>
                                 </tr>
                                 </tbody>
                             </table>
                         </form>

                     </div>
                 </div>
             @endif

             @if($order_three != null)
                 <div class="col-md-4">
                     <div class="box_style_2">
                         <h2 class="inner">Order</h2>
                         <h2 class="inner">{{$order_three->created_at}}</h2>
                         <div id="confirm">
                             <div>
                                 <h4>Order no:  {{$order_three->id}}</h4>
                                 <h4>Customer:  {{$order_three->name}}</h4>
                                 <h4>Address:  {{$order_three->address}}</h4>
                                 <h4>Phone: {{$order_three->cart->addressDetail['phone']}}</h4>
                                 <br>
                                 <h4>Desire Time: {{$order_three->desire_time}}</h4>
                             </div>
                         </div>
                         <h4>Summary</h4>
                         <table class="table table-striped nomargin">
                             <thead>
                             <tr>
                                 <th>Dish</th>
                                 <th>QTY</th>
                             </tr>
                             </thead>
                             <tbody>
                             @foreach($order_three->cart->items as $item)
                                 <tr>
                                     <td>
                                         <strong>{{$item['item']['name']}}</strong>
                                     </td>
                                     <td>
                                         <strong class="pull-right">{{$item['qty']}} </strong>
                                     </td>
                                 </tr>

                             @endforeach
                             </tbody>
                         </table>
                         @if($order_three->cart->addressDetail['Note'] != null)
                             <h4><font color="red">Note:</font></h4>
                             <p>{{$order_three->cart->addressDetail['Note']}}</p>
                         @endif
                         <form id="update_form3" style="margin-top: 20px" action="{{route("admin.order.management.update")}}" method="post">
                         <table class="table">
                             <tbody>
                                   <tr>
                                     <td>
                                         <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                                         <input type="hidden" name="order_id" value="{{$order_three->id}}">
                                         <div class="form-group">
                                             {!! Form::select('status', array('' => 'Select status', '1' => 'Order received','2' => 'Order in kitchen','3' => 'Order on delivery','4' => 'Order Finalised'),$order_three->status, ['class'=>'form-control']) !!}
                                         </div>
                                     </td>
                                     <td>
                                         <button class="btn btn-primary">Update</button>
                                     </td>
                                   </tr>

                             </tbody>
                           </table>
                         </form>
                     </div>
                 </div>
             @endif

        </div>

         @endfor
                <div class="text-center">{{ $orders->links() }}</div>

                <!-- Modal -->
                <div id="alert_modal" class="modal fadeIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Status Notice</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Selected update status cannot be null. Please pick a status before press update button.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
        @else
            <p class="center">Recently, no incoming orders for cooking.</p>
        @endif

 </div><!-- End container -->
 </div>
 <!-- End Content =============================================== -->
 @stop

@section('scripts')

    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });

            $( "#update_form1" ).submit(function( event ) {
                if($('[name="status"]').val() == null || $('[name="status"]').val() == ''){
                    $('#alert_modal').modal('show');
                    return false;
                }

                return true;
            });

            $( "#update_form2" ).submit(function( event ) {
                if($('[name="status"]').val() == null || $('[name="status"]').val() == ''){
                    $('#alert_modal').modal('show');
                    return false;
                }

                return true;
            });

            $( "#update_form3" ).submit(function( event ) {
                if($('[name="status"]').val() == null || $('[name="status"]').val() == ''){
                    $('#alert_modal').modal('show');
                    return false;
                }

                return true;
            });

        });
    </script>
@stop
