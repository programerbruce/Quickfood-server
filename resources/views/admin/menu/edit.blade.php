@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="padding: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">Edit Menu</li>
        </ol>

        @include('includes.form_error')

        {!! $menu->photo ?  '<img src="' . asset($menu->photo) .'" alt="">'
         :
         '<div class="alert alert-secondary" role="alert">
          No picture for Menu now. Pick one :)
        </div>'!!}

        <div style="margin-top: 30px" class="form-group">
            {!! Form::open(['method'=>'POST','action'=>['AdminMenuController@photo'], 'files'=>true]) !!}
                {!! Form::hidden('menu_id', $menu->id) !!}
                {!! Form::label('photo','Photo') !!}
                {!! Form::file('photo', null,['class'=>'form-control']) !!}

                <div style="margin-top: 30px" class="form-group">
                    {!! Form::submit('Change Photo', ['class'=>'btn btn-success']) !!}
                </div>
            {!! Form::close() !!}
        </div>

        {!! Form::open(['method'=>'PATCH','action'=>['AdminMenuController@update',$menu->id]]) !!}

        <div class="form-group">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',$menu->name,['class'=>'form-control']) !!}
        </div>


        <div class="form-group">
            {!! Form::label('category_menus_id','Category') !!}
            {!! Form::select('category_menus_id', [''=>'Choose Category'] + $categories, $menu->category->id, ['class'=>'form-control']) !!}
        </div>


        <div class="form-group">
            {!! Form::label('description','Description') !!}
            @include('includes.editor')
            <textarea name="description" class="form-control my-editor" rows="20">{!! old('description', $menu->description) !!}</textarea>
        </div>

        <div class="form-group">
            {!! Form::label('price','Price') !!}
            {!! Form::number('price',$menu->price,['class'=>'form-control','step'=>'any']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Save', ['class'=>'btn btn-primary','rows'=>3]) !!}
        </div>
        {!! Form::close() !!}

    </div>

@stop