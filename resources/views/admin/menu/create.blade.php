@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">Create Menu</li>
        </ol>

        @include('includes.form_error')

        {!! Form::open(['method'=>'POST','action'=>'AdminMenuController@store', 'files' => true]) !!}

        <div class="form-group">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('category_menus_id','Category') !!}
            {!! Form::select('category_menus_id', [''=>'Choose Category'] + $menuCategories, null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('photo','Photo') !!}
            {!! Form::file('photo',null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description','Description') !!}
            @include('includes.editor')
            <textarea name="description" class="form-control my-editor"></textarea>
        </div>

        <div class="form-group">
            {!! Form::label('price','Price') !!}
            {!! Form::number('price',null,['class'=>'form-control','step'=>'any']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Create Menu', ['class'=>'btn btn-primary']) !!}
        </div>


        {!! Form::close() !!}

    </div>

@stop