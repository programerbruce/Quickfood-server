@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">All Menus</li>
        </ol>
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>id</th>
                <th>photo</th>
                <th>name</th>
                <th>category</th>
                <th>price</th>
                <th>created time</th>
                <th>updated time</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            @if(count($menus) > 0)
                @foreach($menus as $menu)
                    <tbody>
                    <tr>
                        <td>{{$menu->id}}</td>
                        <td>{!! $menu->photo ? '<img height="40" src=' . asset($menu->photo) . ' alt="">' : 'Null'!!}</td>
                        <td>{{$menu->name}}</td>
                        <td>{{$menu->category->name}}</td>
                        <td>{{$menu->price}}</td>
                        <td>{{$menu->created_at->diffForHumans()}}</td>
                        <td>{{$menu->updated_at->diffForHumans()}}</td>
                        <td>
                            <a href="{{route('admin.menu.edit', $menu->id)}}"><button class="btn btn-primary">Edit</button></a>
                        <td>
                            {!! Form::open(['method'=>'DELETE','action'=>['AdminMenuController@destroy', $menu->id]]) !!}

                                    <div class="form-group">
                                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                {!! Form::close() !!}
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            @endif
        </table>
    </div>

@stop
