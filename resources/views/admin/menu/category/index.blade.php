@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">All Categories</li>
        </ol>
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            @if(count($menuCategories) > 0)
                @foreach($menuCategories as $menuCategory)
                    <tbody>
                    <tr>
                        <td>{{$menuCategory->id}}</td>
                        <td>{{$menuCategory->name}}</td>
                        <td>
                            <a href="{{route('admin.menu.category.edit', $menuCategory->id)}}">
                                <button class="btn btn-primary">Edit</button>
                            </a>
                        </td>
                        <td>

                            {!! Form::open(['method'=>'DELETE','action'=>['AdminMenuCategoryController@destroy',$menuCategory->id]]) !!}

                                <div class="form-group">
                                    {!! Form::submit('Delete', ['class'=>'btn btn-danger','rows'=>3]) !!}
                                </div>
                            {!! Form::close() !!}

                        </td>

                    </tr>
                    </tbody>
                @endforeach
            @endif
        </table>
    </div>

@stop
