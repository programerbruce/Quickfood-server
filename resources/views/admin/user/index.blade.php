@extends('admin.layouts.admin')

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });
    </script>
@stop

@section('content')
    <div style="margin: 20px" class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a id="back" href="#">Previous</a>
            </li>
            <li class="breadcrumb-item active">All users</li>
        </ol>
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>role</th>
                <th>img</th>
                <th>email</th>
                <th>status</th>
                <th>created time</th>
                <th>updated time</th>
                <th></th>

            </tr>
            </thead>
            @if(count($users) > 0)
                @foreach($users as $user)
                    <tbody>
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->role->name}}</td>
                        <td>{!! $user->photo ? '<img height="40" src=' . asset($user->photo->file) . ' alt="">' : 'Null'!!}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if($user->is_active == 1)
                                {!! Form::open(['method'=>'PATCH','action'=>['AdminUserController@update', $user->id]]) !!}

                                <input type="hidden" name="is_active" value="0">

                                <div class="form-group">
                                    {!! Form::submit('Un-approve', ['class'=>'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['method'=>'PATCH','action'=>['AdminUserController@update', $user->id]]) !!}

                                <input type="hidden" name="is_active" value="1">

                                <div class="form-group">
                                    {!! Form::submit('Approve', ['class'=>'btn btn-info']) !!}
                                </div>
                                {!! Form::close() !!}
                            @endif
                        </td>
                        <td>{{$user->created_at->diffForHumans()}}</td>
                        <td>{{$user->updated_at->diffForHumans()}}</td>
                        <td>
                            {!! Form::open(['method'=>'DELETE','action'=>['AdminUserController@destroy', $user->id]]) !!}
                                    <div class="form-group">
                                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                {!! Form::close() !!}
                        </td>

                    </tr>
                    </tbody>
                @endforeach
            @endif
        </table>
    </div>

@stop
