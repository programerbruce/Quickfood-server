@extends('blog.index')

@section('css')

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>


    <!-- BASE CSS -->
    <link href="css/base.css" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@stop


@section('sub_header')
    <!-- SubHeader =============================================== -->
    <section class="header-video">
        <div id="hero_video">
            <div id="sub_content">
                <h1>Order Takeaway or Delivery Food</h1>

                <p>
                    Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.
                </p>
                {{--<form method="post" action="list_page.html">--}}
                    {{--<div id="custom-search-input">--}}
                        {{--<div class="input-group">--}}
                            {{--<input type="text" class=" search-query" placeholder="Your Address or postal code">--}}
                            {{--<span class="input-group-btn">--}}
                        {{--<input type="submit" class="btn_search" value="submit">--}}
                        {{--</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</form>--}}
            </div><!-- End sub_content -->
        </div>
        <img src="{{asset('img/sub_header_home.jpg')}}" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="html5" data-video-width="1920" data-video-height="960">

        <div id="count" class="hidden-xs">
            <ul>
                <li><span class="number">2650</span> Orders</li>
                <li><span class="number">5350</span> People Served</li>
                <li><span class="number">12350</span> Registered Users</li>
            </ul>
        </div>
    </section><!-- End Header video -->

    {{--<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/sub_header_cart.jpg')}}" data-natural-width="1400" data-natural-height="350">--}}
        {{--<div id="subheader">--}}
            {{--<div id="sub_content">--}}
                {{--<h1>QuickFood Blog</h1>--}}
                {{--<p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>--}}
                {{--<p></p>--}}
            {{--</div><!-- End sub_content -->--}}
        {{--</div><!-- End subheader -->--}}
    {{--</section><!-- End section -->--}}
@stop

@section('content')

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-12">
                @if(count($posts) > 0)
                @foreach($posts as $post)
                <div class="post">
                    <a href="{{route('blog.post' ,$post->id)}}"><img src="{{asset($post->photos->first()->file)}}" alt="" class="img-responsive"></a>
                    <div class="post_info clearfix">
                        <div class="post-left">
                            <ul>
                                <li><i class="icon-calendar-empty"></i>{{$post->created_at->diffForHumans()}}<em> &nbsp; by {{$post->user->name}}</em></li>
                                <li>{{$post->category->name}}</li>
                            </ul>
                        </div>
                        <div class="post-right"><i class="icon-comment"></i>{{count($post->comments)}} </div>
                    </div>
                    <h2>{{$post->title}}</h2>
                    <p>
                        {!! html_entity_decode($post->body) !!}
                    </p>

                    <a href="{{route('blog.post' ,$post->id)}}" class="btn_1" >Read more</a>
                </div><!-- end post -->
                @endforeach
                @else
                    <p>We will add more posts for you soon!</p>


                @endif

            </div><!-- End col-md-9-->
        </div>

    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-5">
            {{$posts->render()}}
        </div>
    </div>
    <!-- End container -->
@stop

@section('scripts')
    <!-- Modernizr -->
    <script src="{{asset('js/modernizr.js')}}"></script>
    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('js/video_header.js')}}"></script>
    <script>
        $(document).ready(function() {
            'use strict';
            HeaderVideo.init({
                container: $('.header-video'),
                header: $('.header-video--media'),
                videoTrigger: $("#video-trigger"),
                autoPlayVideo: true
            });

        });
    </script>
@stop


