<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="pizza, delivery food, fast food, sushi, take away, chinese, italian food">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>QuickFood - Quality delivery or take away food</title>



    @yield('css')


    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

@include('layouts.partials.head')


<!-- Content ================================================== -->

@yield('content')

<!-- End Content =============================================== -->

<!-- Footer ================================================== -->
@include('layouts.partials.foot')
<!-- End Footer =============================================== -->

<div class="layer"></div><!-- Mobile menu overlay mask -->

<!-- Login modal -->
@include('layouts.partials.loginmodal')
<!-- End modal -->
@include('layouts.partials.registermodal')
<!-- Register modal -->

<!-- End Register modal -->

<!-- COMMON SCRIPTS -->



<script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('js/common_scripts_min.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('assets/validate.js')}}"></script>


@yield('scripts')





</body>
</html>