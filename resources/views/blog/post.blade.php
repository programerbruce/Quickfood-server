@extends('blog.index')

@section('css')

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    <!-- BASE CSS -->
    <link href="{{asset('css/base.css')}}" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="{{asset('css/blog.css')}}" rel="stylesheet">

@stop

@section('sub_header')
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/beef_header.jpg')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>QuickFood Blog</h1>
                <p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>
                <p></p>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
@stop


@section('content')
    <div class="container margin_60_35">
            <div class="col-md-12">
                <div class="post">
                    <img src="{{asset($post->photos->first()->file)}}" alt="" class="img-responsive">
                    <div class="post_info clearfix">
                        <div class="post-left">
                            <ul>
                                <li><i class="icon-calendar-empty"></i>{{$post->created_at->diffForHumans()}} <em> &nbsp; by {{$post->user->name}}</em></li>
                                <li><i class="icon-inbox-alt"></i>{{$post->category->name}}</li>
                            </ul>
                        </div>
                        <div class="post-right"><i class="icon-comment"></i> 25 </div>
                    </div>
                    <h2>{{$post->title}}</h2>
                    {!! html_entity_decode($post->body) !!}
                </div><!-- end post -->

                <h4>{{count($post->comments) > 0 ? count($post->comments) . ' comments' : '' }}</h4>

                @if(count($post->comments) > 0)
                @foreach($post->comments as $comment)
                @if($comment->is_active == 1)
                <div id="comments">
                    <ol>
                        <li>

                            <div class="avatar"><a href="#">{!! $comment->user->photo ? html_entity_decode('<img height="50" src=' . asset($comment->user->photo->file)) . '>' : '<img height="50" src='. asset("img/unlogged_user.png") .'>'!!}</a></div>
                            <div class="comment_right clearfix">

                                By <a href="#">{{$comment->user->name}}</a><span> | </span>{{$comment->created_at->diffForHumans()}}<span> | </span>
                                <a id="reply" onclick='loginmodal("#collapseBox{{ $comment->id }}", {{ Auth::user() ? true : false }})' role="button">Reply</a>
                                <p>
                                    {{$comment->body}}
                                </p>
                                    <div class="comment_info">
                                        <div class="comment-reply-container">

                                        <div class="collapse" id="{!! 'collapseBox' . $comment->id !!}">
                                            {!! Form::open(['method'=>'POST','action'=>'CommentReplyController@store']) !!}
                                            <input type="hidden" name="comment_id", value="{{$comment->id}}">
                                            <div class="form-group">
                                                {!! Form::label('body','Body') !!}
                                                {!! Form::textarea('body',null,['class'=>'form-control','rows'=>3]) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
                                            </div>
                                        {!! Form::close() !!}
                                        <!-- End Nested Comment -->
                                        </div>
                                    </div>

                                </div>

                            </div>



                            @if(count($comment->replies) > 0)
                            <ul>
                                @foreach($comment->replies as $reply)
                                @if($reply->is_active == 1)
                                <li>
                                    <div class="avatar"><a href="#">{!! $reply->get_author->photo ? html_entity_decode('<img height="50" src=' . asset($reply->get_author->photo->file)) . '>' : '<img height="50" src='. asset("img/unlogged_user.png") .'>'!!}</a></div>

                                    <div class="comment_right clearfix">
                                        <div class="comment_info">
                                            By <a href="#">{{$reply->get_author->name}}</a><span>|</span>{{$reply->created_at->diffForHumans()}}
                                        </div>
                                        <p>{{$reply->body}}</p>
                                    </div>
                                </li>
                                @endif
                                @endforeach
                             </ul>

                        @endif
                    </li>

                    </ol>
                </div><!-- End Comments -->
                @endif
                @endforeach
                @endif

                <h4>Leave a comment</h4>
                <div class="row">
                    @include('includes/form_error')
                </div>

                {!! Form::open(['method'=>'POST','action'=>'CommentController@store']) !!}

                <input type="hidden" name="post_id" value={{$post->id}}>


                <div class="form-group">
                    {!! Form::label('comment','Comment') !!}
                    {!! Form::textarea('comment',null,['class'=>'form-control styled', 'rows'=>3,'onfocus' => 'comment_on_focus(' .Auth::user() . ')' ]) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                </div>

                {!! Form::close() !!}


            </div><!-- End col-md-9-->
    </div>

    @include('includes.loginalertmodal')

@stop

@section('position')
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('blog.index')}}">Blog</a></li>
                <li>Post</li>
            </ul>
        </div>
    </div><!-- Position -->
@stop



@section('scripts')

    <script>
        function comment_on_focus(user){
            if(user == null || user == 'undefined'){
                $('#loginModal').modal('show')
                $('[name="body"]').blur()
            }

        }

        function loginmodal(id, is_user){
            if(is_user){
                $(id).slideToggle('slow')
            }else {
                $('#loginModal').modal('show')
            }
        }
    </script>

@stop