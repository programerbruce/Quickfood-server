<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

Dear {{$title}}:<br>
<p>You have place a order in our system. This email to inform you that your order have been received by us and we will prepare your order as soon as possible and you could check your order status through this link
    <a href="{{route('shop.cart.receipt',$receipt_no)}}">Order tracker</a>.</p>
<p>Please also take note of your receipt no({{$receipt_no}}) for future reference.Thanks for choosing us.And we are looking forward to serve you next time.</p>
<br>

Kind regards,<br>
Quickfood team

</body>
</html>