<!--[if lte IE 8]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

<div id="preloader">
    <div class="sk-spinner sk-spinner-wave" id="status">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
</div><!-- End Preload -->

<!-- Header ================================================== -->
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col--md-4 col-sm-4 col-xs-4">
                <a href="{{route('blog.index')}}" id="logo">
                    <img src="{{asset('img/logo.png')}}" width="190" height="23" alt="" data-retina="true" class="hidden-xs">
                    <img src="{{asset('img/logo_mobile.png')}}" width="59" height="23" alt="" data-retina="true" class="hidden-lg hidden-md hidden-sm">
                </a>
            </div>
            <nav class="col--md-8 col-sm-8 col-xs-8">
                <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                <div class="main-menu">
                    <div id="header_menu">
                        <img src="{{asset('img/logo.png')}}" width="190" height="23" alt="" data-retina="true">
                    </div>
                    <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>
                    <ul>
                        <li class="submenu">
                            <a href="{{route('blog.index')}}" class="show-submenu">Blog</a>
                        </li>
                        <li><a href="{{route('shop.menu.index')}}">Menu</a></li>
                        <li><a href="{{route('goToTracker')}}">Tracker</a></li>
                        <li class="submenu">
                        @if(Auth::user())
                            <a href="#">{{Auth::user()->name}}</a>
                                <ul>
                                    @if(Auth::user()->role_id == 1)
                                    <li><a href="{{route('admin.dashboard')}}">Admin pannel</a></li>
                                    @endif
                                    <li><a href="{{route('shop.user.index')}}">Profile</a></li>
                                    <li><a href="{{route('shop.user.orders',Auth::user()->id)}}">Orders</a></li>
                                    <li><a href="{{route('logout')}}">Log out</a></li>
                                </ul>
                        @else
                            <a href="{{route('login')}}">Login</a>
                        @endif
                        </li>

                    </ul>
                </div><!-- End main-menu -->
            </nav>
        </div><!-- End row -->
    </div><!-- End container -->
</header>


<!-- End Header =============================================== -->

<!-- SubHeader =============================================== -->
@yield('sub_header')
<!-- End SubHeader ============================================ -->

@yield('position')
