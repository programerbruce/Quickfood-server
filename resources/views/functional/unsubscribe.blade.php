@extends('shop.layouts.index')


@section('sub_header')
    <section class="parallax-window"  id="short"  data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Email Unsubscribe</h1>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
@stop

@section('content')
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">
            @if(session('unsubscribe_info'))
                <div class="alert alert-success" role="alert">
                    {{session('unsubscribe_info')}}
                </div>
            @endif
            <div class="col-md-4">
            </div>
            <div class="col-md-4"  id="newsletter">
                <h3>Unsubscribe Newsletter</h3>
                <div id=    "message-newsletter_2"></div>
                <form method="post" action="{{route('unsubscribe')}}" name="unsubscribe_form">
                    <input size="4" name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />

                    <div class="form-group">
                        <input name="email_newsletter_2" id="email_newsletter_2"  type="email" value=""  placeholder="Your mail" class="form-control">
                    </div>
                    <input type="submit" value="Unsubscribe" class="btn_1" id="submit-unsubscribe_form">
                </form>
            </div>
        </div>
    </div><!-- End container -->
    <!-- End Content =============================================== -->
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v2/"></script>
    <script src="{{asset('js/stripe/checkout.js')}}"></script>
@stop

