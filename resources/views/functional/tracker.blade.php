@extends('shop.layouts.index')

@section('css')

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>


    <!-- BASE CSS -->
    <link href="css/base.css" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@stop

@section('sub_header')
    <!-- SubHeader =============================================== -->
    <section class="header-video">
        <div id="hero_video">
            <div id="sub_content">
                <h1>Order Tracker</h1>

                <p>
                    Type you order no and track it
                </p>
                <div id="custom-search-input">
                <div class="input-group">
                <input id="order_no" type="text" class=" search-query" placeholder="Type order number here">
                <span class="input-group-btn">
                <input id="order_no_submit" type="submit" class="btn_search" value="submit">
                </span>
                </div>
                </div>
            </div><!-- End sub_content -->
        </div>
        <img src="{{asset('img/sub_header_home.jpg')}}" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="html5" data-video-width="1920" data-video-height="960">

        {{--<div id="count" class="hidden-xs">--}}
            {{--<ul>--}}
                {{--<li><span class="number">2650</span> Restaurant</li>--}}
                {{--<li><span class="number">5350</span> People Served</li>--}}
                {{--<li><span class="number">12350</span> Registered Users</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </section><!-- End Header video -->

    {{--<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/sub_header_cart.jpg')}}" data-natural-width="1400" data-natural-height="350">--}}
    {{--<div id="subheader">--}}
    {{--<div id="sub_content">--}}
    {{--<h1>QuickFood Blog</h1>--}}
    {{--<p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>--}}
    {{--<p></p>--}}
    {{--</div><!-- End sub_content -->--}}
    {{--</div><!-- End subheader -->--}}
    {{--</section><!-- End section -->--}}
@stop

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Content ================================================== -->
    <div id="order_container" class="container margin_60_35">

        <P>Please type your order no into search box</P>


    </div>

    <!-- Modal -->
    <div id="tracker_alert_modal" class="modal fade" id="tracker_alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Order no</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Please type an order no!!!!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@stop

@section('scripts')
    <!-- Modernizr -->
    <script src="{{asset('js/modernizr.js')}}"></script>
    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('js/video_header.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('#order_no_submit').click(function(){
                var order_no = $('#order_no').val();
                if((order_no == null) || (order_no == '')){
                    $('#tracker_alert_modal').modal('show');
                    return;
                }

                renderPage(order_no);

            });

            'use strict';
            HeaderVideo.init({
                container: $('.header-video'),
                header: $('.header-video--media'),
                videoTrigger: $("#video-trigger"),
                autoPlayVideo: true
            });

        });



        function renderPage(order_no) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // alert('Menu id' + menuId);
            $.ajax({
                type: 'POST',
                url: 'ajaxRenderTracker',
                data: {"order_no": order_no},
                dataType: "json",
                success: function (data) {
                    var orders = data;

                    console.log('Hahahaha');
                    var html = '';

                    for (i = 0; i < orders.length; i += 3) {

                        var order_one = null;
                        var order_two = null;
                        var order_three = null;

                        if (orders[i]) {
                            order_one = orders[i];
                        }

                        if ((i + 1) < orders.length) {
                            order_two = orders[i + 1];
                        }

                        if ((i + 2) < orders.length) {
                            order_three = orders[i + 2];
                        }
                        html += '<div class="row">';
                        if (order_one != null) {
                            var status = '';
                            if(order_one.status == 1){
                                status = 'Order Received'
                            }else if(order_one.status == 2){
                                status = 'Order Preparing'
                            }else if(order_one.status == 3){
                                status = 'Order on delivery'
                            }else if(order_one.status == 4){
                                status = 'Order finish'
                            }

                            html += '<div class="col-md-4">' +
                                '<div class="box_style_2">' +
                                '<h2 class="inner">Order no:<br>  <font style="font-size: 15px">' + order_one.payment_id + '</font></h2>' +
                                '<h2 class="inner">Status:<br> <font style="font-size: 15px">'+ status +'</font></h2>' +
                                '<h2 class="inner">' + order_one.created_at +'</h2>' +
                                '<div id="confirm">' +
                                '<h4>Order no: ' +  order_one.id + '</h4>' +
                                '<div>' +
                                '<h4>Customer: ' + order_one.name + '</h4>' +
                                '<h4>Address: ' + order_one.address + '</h4>' +
                                '<h4>Phone: ' + order_one.cart.addressDetail['phone'] + '</h4>' +
                                '<br>' +
                                '<h4>Desire Time: ' + order_one.desire_time + '</h4>' +
                                '</div>' +
                                '</div>' +
                                '<h4>Summary</h4>' +
                                '<table class="table table-striped nomargin">' +
                                '<thead>' +
                                '<tr>' +
                                '<th>Dish</th>' +
                                '<th>QTY</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>';
                            $.each(order_one.cart.items, function (index, item) {
                                html += '<tr>' +
                                    '<td>' +
                                    '<strong>' + item['item']['name'] +
                                    '</td>' +
                                    '<td>' +
                                    '<strong class="pull-right">' + item['qty'] + '</strong>' +
                                    '</td>' +
                                    '</tr>'
                            });
                            html += '</tbody>' +
                                '</table>';
			     html += '<br>' +
                            '<h4><lable>Total: $</lable>' + order_one.amount + '</h4>' +
                            '<br>';
                            if(order_one.cart.addressDetail['Note'] != null){
                                html += '<h4><font color="red">Note:</font></h4>'+
                                    '<p>' + order_one.cart.addressDetail['Note'] + '</p>';
                            }

                            html += '<form action="{{route("admin.order.ajaxUpdateOrderToDelivery")}}" method="post">' +
                                '<input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />' +
                                '<input type="hidden" name="order_id" value="'+ order_one.id + '"/>' +
                                '</form>';

                            html += '</div></div>';
                        }

                        if (order_two != null) {
                            var status = '';
                            if(order_two.status == 1){
                                status = 'Order Received'
                            }else if(order_two.status == 2){
                                status = 'Order Preparing'
                            }else if(order_two.status == 3){
                                status = 'Order on delivery'
                            }else if(order_two.status == 4){
                                status = 'Order finish'
                            }
                            html += '<div class="col-md-4">' +
                                '<div class="box_style_2">' +
                                '<h2 class="inner">'+ status +'</h2>' +
                                '<h2 class="inner">' + order_two.created_at +'</h2>' +
                                '<div id="confirm">' +
                                '<h4>Order no: ' +  order_two.id + '</h4>' +
                                '<h4>Customer: ' + order_two.name + '</h4>' +
                                '<h4>Address: ' + order_two.address + '</h4>' +
                                '<h4>Phone: ' + order_two.cart.addressDetail['phone'] + '</h4>' +
                                '<br>' +
                                '<h4>Desire Time: ' + order_two.desire_time + '</h4>' +
                                '</div>' +
                                '<h4>Summary</h4>' +
                                '<table class="table table-striped nomargin">' +
                                '<thead>' +
                                '<tr>' +
                                '<th>Dish</th>' +
                                '<th>QTY</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>';
                            $.each(order_two.cart.items, function (index, item) {
                                html += '<tr>' +
                                    '<td>' +
                                    '<strong>' + item['item']['name'] +
                                    '</td>' +
                                    '<td>' +
                                    '<strong class="pull-right">' + item['qty'] + '</strong>' +
                                    '</td>' +
                                    '</tr>'
                            });
                            html += '</tbody>' +
                                '</table>';
                            if(order_two.cart.addressDetail['Note'] != null){
                                html += '<h4><font color="red">Note:</font></h4>'+
                                    '<p>' + order_two.cart.addressDetail['Note'] + '</p>';
                            }
                            html += '<form action="{{route("admin.order.ajaxUpdateOrderToDelivery")}}" method="post">' +
                                '<input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />' +
                                '<input type="hidden" name="order_id" value="'+ order_two.id + '"/>' +
                                '</form>';

                            html += '</div></div>';
                        }


                        if (order_three != null) {
                            var status = '';
                            if(order_three.status == 1){
                                status = 'Order Received'
                            }else if(order_three.status == 2){
                                status = 'Order Preparing'
                            }else if(order_three.status == 3){
                                status = 'Order on delivery'
                            }else if(order_three.status == 4){
                                status = 'Order finish'
                            }
                            html += '<div class="col-md-4">' +
                                '<div class="box_style_2">' +
                                '<h2 class="inner">'+ status +'</h2>' +
                                '<h2 class="inner">' + order_three.created_at +'</h2>' +
                                '<div id="confirm">' +
                                '<h4>Order no: ' +  order_three.id + '</h4>' +
                                '<h4>Customer: ' + order_three.name + '</h4>' +
                                '<h4>Address: ' + order_three.address + '</h4>' +
                                '<h4>Phone: ' + order_three.cart.addressDetail['phone'] + '</h4>' +
                                '<br>' +
                                '<h4>Desire Time: ' + order_three.desire_time + '</h4>' +
                                '</div>' +
                                '<h4>Summary</h4>' +
                                '<table class="table table-striped nomargin">' +
                                '<thead>' +
                                '<tr>' +
                                '<th>Dish</th>' +
                                '<th>QTY</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>';
                            $.each(order_three.cart.items, function (index, item) {
                                html += '<tr>' +
                                    '<td>' +
                                    '<strong>' + item['item']['name'] +
                                    '</td>' +
                                    '<td>' +
                                    '<strong class="pull-right">' + item['qty'] + '</strong>' +
                                    '</td>' +
                                    '</tr>'
                            });
                            html += '</tbody>' +
                                '</table>';
                            if(order_three.cart.addressDetail['Note'] != null){
                                html += '<h4><font color="red">Note:</font></h4>'+
                                    '<p>' + order_three.cart.addressDetail['Note'] + '</p>';
                            }
                            html += '<form action="{{route("admin.order.ajaxUpdateOrderToDelivery")}}" method="post">' +
                                '<input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />' +
                                '<input type="hidden" name="order_id" value="'+ order_three.id + '"/>' +
                                '</form>';

                            html += '</div></div>';
                        }


                        html += '</div>';

                    }

                    if(orders.length == 0){
                        html += '<p class="center">Typed order_no not exist in our system.</p>';
                    }




                    $('#order_container').html(html);

                }
            });
        }

    </script>
@stop
