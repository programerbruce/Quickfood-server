

<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>RoyalPay PHP Demo - QRCode</title>
    <script>
        function redirect(url) {
            window.location.href = url;
        }
    </script>
	<style>
	  .lnk{
	      display:block;
		  width:210px;
		  height:50px;
		  border-radius:15px;
		  background-color:#fe6714;
		  color:#fff;
		  text-align:center;
		  font-size:16px;
		  line-height:50px;
		  text-decoration:none;
		  margin:15px auto;
	  }
	</style>
</head>
<body>
<?php

use App\lib\RoyalPayApi;
use App\lib\RoyalPayConfig;
use App\lib\RoyalPayExchangeRate;
use App\lib\RoyalPayRedirect;
use App\lib\RoyalPayUnifiedOrder;

ini_set('date.timezone', 'Asia/Shanghai');

header("Content-Type:text/html;charset=utf-8");
/**
 * 流程：
 * 1、创建QRCode支付单，取得code_url，生成二维码
 * 2、用户扫描二维码，进行支付
 * 3、支付完成之后，RoyalPay服务器会通知支付成功
 * 4、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
 */
//获取扫码
if(!isset($_GET["channel"])){
  ?>
  <a href="qr.php?channel=Wechat" class="lnk" >Wechat</a>
  <a href="qr.php?channel=Alipay" class="lnk" >Alipay</a>
<?php
}else{
$channel = $_GET["channel"];
$input = new RoyalPayUnifiedOrder();
$input->setOrderId(RoyalPayConfig::PARTNER_CODE . date("YmdHis"));
$input->setDescription("test");
$input->setChannel($channel);
$input->setPrice("1");
$input->setCurrency("AUD");
$input->setNotifyUrl("http://115.29.162.214/example/notify.php");
$input->setOperator("123456");
$currency = $input->getCurrency();
if (!empty($currency) && $currency == 'CNY') {
    //建议缓存汇率,每天更新一次,遇节假日或其他无汇率更新情况,可取最近一个工作日的汇率
    $inputRate = new RoyalPayExchangeRate();
    $rate = RoyalPayApi::exchangeRate($inputRate);
    if ($rate['return_code'] == 'SUCCESS') {
        $real_pay_amt = $input->getPrice() / $rate['rate'] / 100;
        if ($real_pay_amt < 0.01) {
            echo 'CNY amount shall be more than 0.01 AUD for current exchange rate';
            exit();
        }
    }
}

$result = RoyalPayApi::qrOrder($input);
$url2 = $result["code_url"];
//echo $url2;

//跳转 redirect
$inputObj = new RoyalPayRedirect();
$inputObj->setRedirect(urlencode('http://115.29.162.214/example/success.php?order_id=' . strval($input->getOrderId())));
?>
<div style="margin-left: 10px;color:#556B2F;font-size:30px;font-weight: bolder;">Method One: Customized html page</div>
<br/>
<img alt="扫码支付" src="http://localhost/deliverylog/public/shop/qrcode" style="width:150px;height:150px;"/>
<div style="margin-left: 10px;color:#556B2F;font-size:30px;font-weight: bolder;">Method Two: Redirect to RoyalPay QRCode gateway page</div>
<br/>
<button onclick="redirect('<?php echo RoyalPayApi::getQRRedirectUrl($result['pay_url'], $inputObj); ?>')">Redirect
</button>
<?php
  }
?>
</body>
</html>