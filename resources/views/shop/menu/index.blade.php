@extends('shop.layouts.index')

@section('sub_header')
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->

@stop


@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        @if(Session::has('cart_empty'))
        <div class="alert alert-info" role="alert">
            {{session('cart_empty')}}
        </div>
        @endif
        <div class="row">

            <div class="col-md-3">
                {{--<div class="box_style_1">--}}
                    {{--@if(count($categories) > 0)--}}
                    {{--<ul id="cat_nav">--}}
                        {{--@foreach($categories as $category)--}}
                        {{--<li><a href="#starters" class="active">{{$category->name}} <span>({{count($category->menus)}})</span></a></li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                    {{--@endif--}}
                {{--</div><!-- End box_style_1 -->--}}

                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">9090980</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>
            </div><!-- End col-md-3 -->

            <div class="col-md-6">
                <div class="box_style_2" id="main_menu">
                    <h2 class="inner">Menu</h2>
                    @if(count($categories) > 0)
                        @foreach($categories as $category)
                            @if(count($category->menus) > 0)
                                <table class="table table-striped cart-list">
                                    <h3 style="margin-top: 50px" class="nomargin_top" id="starters">{{$category->name}}</h3>
                                <thead>
                                <tr>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Order
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($category->menus as $menu)
                                    <tr>
                                        <td>
                                            <figure class="thumb_menu_list"><img src="{{asset($menu->photo)}}" alt="thumb"></figure>
                                            {!! html_entity_decode($menu->description) !!}
                                        </td>
                                        <td>
                                            <strong>$ {{$menu->price}}</strong>
                                        </td>
                                        <td class="options">
                                            <a onclick='addToCart({{$menu->id}})'>
                                                <i class="icon_plus_alt2"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                        @else
                            <p>Currently out menu list is empty. We will add more items into it soon.</p>
                    @endif
                        </tbody>
                    </table>
                    <hr>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box" >
                        <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                        @if(Session::has('cart'))
                        <table class="table table_summary">
                            @if(count(session('cart')->items) > 0)
                            <tbody>
                            @foreach(session('cart')->items as $item)
                            <tr>
                                <td>
                                    <a onclick='minusFromCart({{$item['id']}})' href="#" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>{{$item['qty']}} x</strong> {{$item['item']->name}}
                                </td>
                                <td>
                                    <strong class="pull-right">$ {{$item['price']}}</strong>
                                </td>
                            </tr>

                            </tbody>
                            @endforeach
                            @endif
                        </table>
                        @endif
                        <hr>
                        @if(Session::has('cart'))
                            <div class="row" id="options_2">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="1" {{Session::get('cart')->choice == 1 ? 'checked' : ''}} name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="2" name="option_2" {{Session::get('cart')->choice == 2 ? 'checked' : ''}} class="form-check-input"><span style="padding: 10px">Take Away</span></label>
                                </div>
                            </div><!-- Edn options 2 -->
                            <hr>
                            <table class="table table_summary">
                                <tbody>
                                @if(session('cart')->choice == 1)
                                <tr>
                                    <td>
                                        Delivery fee <span class="pull-right">$10</span>

                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <td class="total">
                                        TOTAL <span class="pull-right">{{ session('cart')->choice == 1 ? session('cart')->totalPrice + 10 : session('cart')->totalPrice }}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @else
                            <div class="row" id="options_2">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="1" checked name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="2" name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label>
                                </div>
                            </div><!-- Edn options 2 -->
                            <hr>
                            <table class="table table_summary">
                                <tbody>
                                <tr>
                                    <td class="total" colspan="2">
                                        TOTAL <span class="pull-right">{{ Session::has('cart') ? session('cart')->totalPrice + 10 : '$0' }}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif



                        @if(Session::has('cart'))

                            <form id="cart_form" action="{{route('shop.cart.updateChoice')}}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id="service_type" name="choice" value="">
                            </form>
                            <a id='order_process_btn' class="btn_full" type="button">Order now</a>
                        @endif

                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->
@stop

@section('scripts')

    <script>
        if(!!window.performance && window.performance.navigation.type === 2)
        {
            console.log('Reloading');
            window.location.reload();
        }
    </script>

    <script>
        $(document).on('click','#order_process_btn',function(){

            var radioVal = $("input[name='option_2']:checked").val();

            // alert('Clicked hehe:' + radioVal);
            $('#service_type').val(radioVal);
            $( "#cart_form" ).submit();
        });
    </script>

    <script>


        function addToCart(menuId){

            radioVal = $("input[name='option_2']:checked").val();

            // alert(radioVal);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // alert('Menu id' + menuId);
            $.ajax({
                type:'POST',
                url:'cart/add ',
                data: { 'menu_id':  menuId, 'choice': radioVal, 'qty': 1 },
                dataType : "json",
                success:function(data){

                    console.log(data);

                    var html = '<h3>Your order <i class="icon_cart_alt pull-right"></i></h3> ';

                   Object.keys(data.items).forEach(function(key){

                        // console.log(item);
                            item = data.items[key];

                            html += '<table class="table table_summary"> ' +
                            '<tbody><tr> ' +
                            '<td> ' +
                            '<a onclick="minusFromCart(' +key +')"' +'href="#" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>' +
                            item.qty +
                            '</strong>' +
                            item.item.name +
                            '</td> <td> ' +
                            '<strong class="pull-right"> $' + item.price + '</strong> </td> </tr>';

                    });



                    html = html + '</tbody></table> <hr> ';
                    if(data.choice == 1) {
                        html += '<div class="row" id="options_2"> <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                        '<label><input type="radio" value="1" checked name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label> </div> ' +
                        '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                        '<label><input type="radio" value="2" name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label> ' +
                        '</div> </div>'
                        }else{
                        html += '<div class="row" id="options_2"> <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                        '<label><input type="radio" value="1"  name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label> </div> ' +
                        '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                        '<label><input type="radio" value="2" checked name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label> ' +
                        '</div> </div>'
                        }
                    html += ' <hr>';

                     html = html + '<table class="table table_summary"> <tbody>';
                     if(data.choice == 1){
                         html +=  ' <tr><td>Delivery fee <span class="pull-right">$10</span></td></tr><tr> <td class="total">TOTAL ' +
                         '<span class="pull-right">$ ' + (parseFloat(data.totalPrice) + 10) +'</span>' +
                         '</td> </tr> ' +
                         '</tbody> ' +
                         '</table> ' +
                         '<form id="cart_form" action="{{route('shop.cart.updateChoice')}}" method="post">' +
                         '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                         '                                <input type="hidden" id="service_type" name="choice" value="">' +
                         '                            </form>' +
                         '                            <a id="order_process_btn" class="btn_full" type="button">Order now</a>';
                     }else{
                         html +=  ' <tr> <td class="total">TOTAL ' +
                             '<span class="pull-right">$ ' + data.totalPrice +'</span>' +
                             '</td> </tr> ' +
                             '</tbody> ' +
                             '</table> ' +
                             '<form id="cart_form" action="{{route('shop.cart.updateChoice')}}" method="post">' +
                             '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                             '                                <input type="hidden" id="service_type" name="choice" value="">' +
                             '                            </form>' +
                             '                            <a id="order_process_btn" class="btn_full" type="button">Order now</a>';
                     }


                    // console.log(html);

                    $('#cart_box').html(html);


            }
            }
            );
        }



        function minusFromCart(menuId){

            radioVal = $("input[name='option_2']:checked").val();

            // alert(radioVal);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // alert('Menu id' + menuId);
            $.ajax({
                type:'POST',
                url:'cart/minus',
                data: { 'menu_id': menuId, 'choice': radioVal, 'qty': 1 },
                dataType : "json",
                success:function(data){

                    console.log(data);
                    if(Object.keys(data.items).length > 0) {
                        var html = '<h3>Your order <i class="icon_cart_alt pull-right"></i></h3> ';


                        Object.keys(data.items).forEach(function (key) {

                            // console.log(item);
                            item = data.items[key];

                            html += '<table class="table table_summary"> ' +
                                '<tbody><tr> ' +
                                '<td> ' +
                                '<a onclick="minusFromCart(' + key + ')"' + 'href="#" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>' +
                                item.qty +
                                '</strong>' +
                                item.item.name +
                                '</td> <td> ' +
                                '<strong class="pull-right"> $' + item.price + '</strong> </td> </tr>';

                        });


                        html = html + '</tbody></table> <hr> ';
                        if (data.choice == 1) {
                            html += '<div class="row" id="options_2"> <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                                '<label><input type="radio" value="1" checked name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label> </div> ' +
                                '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                                '<label><input type="radio" value="2" name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label> ' +
                                '</div> </div>'
                        } else {
                            html += '<div class="row" id="options_2"> <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                                '<label><input type="radio" value="1"  name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label> </div> ' +
                                '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6"> ' +
                                '<label><input type="radio" value="2" checked name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label> ' +
                                '</div> </div>'
                        }
                        html += ' <hr>';

                        html = html + '<table class="table table_summary"> <tbody>';
                        if(data.choice == 1){
                            html +=  ' <tr><td>Delivery fee <span class="pull-right">$10</span></td></tr><tr> <td class="total">TOTAL ' +
                                '<span class="pull-right">$ ' + (parseFloat(data.totalPrice) + 10) +'</span>' +
                                '</td> </tr> ' +
                                '</tbody> ' +
                                '</table> ' +
                                '<form id="cart_form" action="{{route('shop.cart.updateChoice')}}" method="post">' +
                                '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                                '                                <input type="hidden" id="service_type" name="choice" value="">' +
                                '                            </form>' +
                                '                            <a id="order_process_btn" class="btn_full" type="button">Order now</a>';
                        }else{
                            html +=  ' <tr> <td class="total">TOTAL ' +
                                '<span class="pull-right">$ ' + data.totalPrice +'</span>' +
                                '</td> </tr> ' +
                                '</tbody> ' +
                                '</table> ' +
                                '<form id="cart_form" action="{{route('shop.cart.updateChoice')}}" method="post">' +
                                '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                                '                                <input type="hidden" id="service_type" name="choice" value="">' +
                                '                            </form>' +
                                '                            <a id="order_process_btn" class="btn_full" type="button">Order now</a>';
                        }

                        // console.log(html);

                        $('#cart_box').html(html);
                    }else{
                        var html = '      <hr>\n' +
                            '                        <div class="row" id="options_2">\n' +
                            '                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">\n' +
                            '                                <label><input type="radio" value="1" checked name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label>\n' +
                            '                            </div>\n' +
                            '                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">\n' +
                            '                                <label><input type="radio" value="2" name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label>\n' +
                            '                            </div>\n' +
                            '                        </div><!-- Edn options 2 -->\n' +
                            '\n' +
                            '                        <hr>\n' +
                            '                        <table class="table table_summary">\n' +
                            '                            <tbody>\n' +
                            '                            <tr>\n' +
                            '                                <td>\n' +
                            '                                    Delivery fee <span class="pull-right">$10</span>\n' +
                            '                                </td>\n' +
                            '                            </tr>\n' +
                            '                            <tr>\n' +
                            '                                <td class="total">\n' +
                            '                                    TOTAL <span class="pull-right">$0</span>\n' +
                            '                                </td>\n' +
                            '                            </tr>\n' +
                            '                            </tbody>\n' +
                            '                        </table>\n' +
                            '\n' +
                            '                    </div><!-- End cart_box -->';

                        $('#cart_box').html(html);
                    }
                }
            }
        );
    }


    </script>


@stop