@extends('shop.layouts.index')

@section('css')

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="css/contact.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/lib/animate.css" media="screen, projection" />

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>

    <!-- BASE CSS -->
    <link href="{{asset('css/base.css')}}" rel="stylesheet">

    <!-- Radio and check inputs -->
    <link href="{{asset('css/skins/square/grey.css')}}" rel="stylesheet">




@stop


@section('sub_header')
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Contact us</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->

@stop

@section('content')
    @if(session('contact_save_status'))
        <div class="alert alert-success" role="alert">
            {{session('contact_save_status')}}
        </div>
    @endif
    <div class="container margin_60_35">
        @include('includes.form_error')
        <div id="contact">
            <div class="container">
                <div class="section_header">
                    <h3>Get in touch</h3>
                </div>
                <div class="row contact">
                    <p>
                        We’d love to hear from you. Interested in working together? Fill out the form below with some info about your project and I will get back to you as soon as I can. Please allow a couple days for me to respond.</p>

                    <form action="{{route('saveContact')}}" method="post">
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />

                        <div class="row form">
                            <div class="col-sm-6 row-col">
                                <div class="box">
                                    <input name="name" class="name form-control" type="text" placeholder="Name">
                                    <input name="email" class="mail form-control" type="text" placeholder="Email">
                                    <input name="phone" class="phone form-control" type="text" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="box">
                                    <textarea name="message" placeholder="Type a message here..." class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row submit">
                            <div class="col-md-3 right">
                                <input type="submit" value="Send your message">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <div id="map"></div>

        </div>
    </div>
@stop

@section('scripts')

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVgQAacDWC008YlWPjp22BgtsaP46-hRU"></script>


    <script>

        $(function () {

            function initMap() {

                var location = new google.maps.LatLng(-37.81361100000001, 144.96305600000005);

                var mapCanvas = document.getElementById('map');
                var mapOptions = {
                    center: location,
                    zoom: 16,
                    panControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(mapCanvas, mapOptions);

                var markerImage = 'img/marker.png';

                var marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: markerImage
                });

                var contentString = '<div class="info-window">' +
                    '<h3>Quickfood company</h3>' +
                    '<div class="info-content">' +
                    '<p>Our company located in the center of Melbourne CBD, we are happy to mee you in our office.</p>' +
                    '</div>' +
                    '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 400
                });

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

                // var styles = [{"featureType": "landscape", "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]}, {"featureType": "poi", "stylers": [{"saturation": 100}, {"lightness": 51}, {"visibility": "simplified"}]}, {"featureType": "road.highway", "stylers": [{"saturation": -100}, {"visibility": "simplified"}]}, {"featureType": "road.arterial", "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]}, {"featureType": "road.local", "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]}, {"featureType": "transit", "stylers": [{"saturation": -100}, {"visibility": "simplified"}]}, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "labels", "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]}];
                //
                // map.set('styles', styles);


            }

            google.maps.event.addDomListener(window, 'load', initMap);


        });


    </script>

    <script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/common_scripts_min.js')}}"></script>
    <script src="{{asset('js/functions.js')}}"></script>
    <script src="{{asset('assets/validate.js')}}"></script>

@stop


