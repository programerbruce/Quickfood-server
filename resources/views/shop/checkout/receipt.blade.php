@extends('shop.layouts.index')


@section('sub_header')
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_2.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#0" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@stop

@section('content')
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="box_style_2">
                    <h2 class="inner">Order confirmed!</h2>
                    <p class="font-weight-light">Order No: <a>{{$order->payment_id}}</a></p>
                    <div id="confirm">
                        <i class="icon_check_alt2"></i>
                        <h3>Thank you!</h3>
                        <p>
                            Your order has been placed. And you can check your order status on this page or go to <a href="#">order tracker</a> page to track order with order id.
                        </p>
                    </div>
                    <h4>Summary</h4>
                    <table class="table table-striped nomargin">
                        <tbody>
                        @foreach($cart->items as $item)
                        <tr>
                            <td>
                                <strong>{{$item['qty']}} x </strong> {{$item['item']['name']}}
                            </td>
                            <td>
                                <strong class="pull-right">${{$item['price']}}</strong>
                            </td>
                        </tr>
                        @endforeach

                        @if($order->expect_time)
                        <tr>
                            <td>
                                {{$cart->choice == 1 ? 'Delivery' : 'Takeaway'}} schedule <a href="#" class="tooltip-1" data-placement="top" title="" data-original-title="Please consider 30 minutes of margin for the delivery!"><i class="icon_question_alt"></i></a>
                            </td>
                            <td>
                                <strong class="pull-right">{{$order->expect_time}}</strong>
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td class="total_confirm">
                                TOTAL
                            </td>
                            <td class="total_confirm">
                                <span class="pull-right">${{$order->amount}}</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v2/"></script>
    <script src="{{asset('js/stripe/checkout.js')}}"></script>
@stop


