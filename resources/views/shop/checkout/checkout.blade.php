@extends('shop.layouts.index')


@section('sub_header')
    <section class="parallax-window"  id="short"  data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#0" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_3.html" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
@stop

@section('content')
    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div id="charge_error" class="alert alert-danger {{ !Session::has('charge_error') ? 'hidden' : '' }}" role="alert">
            {{--{{ Session::has('charge_error') ? session('charge_error') : ''  }}--}}
        </div>

        @if(session('charge_error'))
        <div class="alert alert-danger" role="alert">
            {{ Session::has('charge_error') ? session('charge_error') : ''  }}
        </div>
        @endif
        <div class="row">
            {{--<form action="{{route('shop.cart.checkout')}}" method="post" id="checkout-form">--}}
                {!! Form::open(['id'=> 'checkout-form','method'=>'POST','action'=>'CartController@checkout']) !!}

                <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />

                <div class="col-md-3">
                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                </div><!-- End box_style_2 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>
            </div><!-- End col-md-3 -->

            <div class="col-md-6">
                <div class="box_style_2">
                    <h2 class="inner">Payment methods</h2>
                    <div id="credit_btn" class="payment_select">
                        <label><input style="margin-right: 30px" type="radio" checked value="1" name="paymentMethod" class="form-check-input">Credit card</label>
                        <i class="icon_creditcard"></i>
                    </div>
                    <div id="wechat_btn" class="payment_select">
                        <label><input style="margin-right: 30px" type="radio" value="2" name="paymentMethod" class="form-check-input">Wechat Pay</label>
                        <i><img style="height: 30px" src="{{asset('img/wechatpay.png')}}" alt=""></i>
                    </div>
                    <div id="ali_btn" class="payment_select">
                        <label><input style="margin-right: 30px" type="radio" value="3"  name="paymentMethod" class="form-check-input">Ali Pay</label>
                        <i><img style="height: 30px" src="{{asset('img/alipay.png')}}" alt=""></i>
                    </div>

                    <div id="credit_card_form">
                        <div class="form-group">
                            <label>Name on card</label>
                            <input required type="text" class="form-control" id="name_card_order" name="name_card_order" placeholder="First and last name">
                        </div>
                        <div class="form-group">
                            <label>Card number</label>
                            <input required type="text" id="card_number" name="card_number" class="form-control" placeholder="Card number">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Expiration date</label>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input required type="text" id="expire_month" name="expire_month" class="form-control" placeholder="mm">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input required type="text" id="expire_year" name="expire_year" class="form-control" placeholder="yyyy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label>Security code</label>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <input required type="text" id="ccv" name="ccv" class="form-control" placeholder="CCV">
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-6">
                                            <img src="{{asset('img/icon_ccv.gif')}}" width="50" height="29" alt="ccv"><small>Last 3 digits</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--End row -->
                    {{--<div class="payment_select" id="paypal">--}}
                        {{--<label><input type="radio" value="" name="payment_method" class="icheck">Pay with paypal</label>--}}
                    {{--</div>--}}
                    {{--<div class="payment_select nomargin">--}}
                        {{--<label><input type="radio" value="" name="payment_method" class="icheck">Pay with cash</label>--}}
                        {{--<i class="icon_wallet"></i>--}}
                    {{--</div>--}}
                </div>
                <!-- End box_style_1 -->


            </div><!-- End col-md-6 -->

                <div class="col-md-3" id="sidebar">
                    <div class="theiaStickySidebar">
                        <div id="cart_box" >
                            <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                            @if(Session::has('cart'))
                                <table class="table table_summary">
                                    @if(count(session('cart')->items) > 0)
                                        <tbody>
                                        @foreach(session('cart')->items as $item)
                                            <tr>
                                                <td>
                                                    <strong>{{$item['qty']}}x</strong> {{$item['item']->name}}
                                                </td>
                                                <td>
                                                    <strong class="pull-right">$ {{$item['price']}}</strong>
                                                </td>
                                            </tr>

                                        </tbody>
                                        @endforeach
                                    @endif
                                </table>
                            @endif
                            <hr>
                            @if(Session::has('cart'))
                                <div class="row" id="options_2">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                        <label><span>{{Session::get('cart')->choice == '1'? 'Delivery' : 'Take Away'}}</span></label>
                                    </div>

                                </div><!-- Edn options 2 -->
                            @else
                                <div class="row" id="options_2">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                        <label><input type="radio" value="1" checked name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                        <label><input type="radio" value="2" name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label>
                                    </div>
                                </div><!-- Edn options 2 -->
                            @endif
                            <hr>
                            <table class="table table_summary">
                                <tbody>
                                @if(Session::get('cart')->choice == '1')
                                <tr>
                                    <td>
                                        Delivery fee <span class="pull-right">$10</span>
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <td class="total">
                                        TOTAL <span class="pull-right">$ {{ Session::get('cart')->choice == '1' ? session('cart')->totalPrice + 10 : session('cart')->totalPrice }}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>


                            <button id="credit_submit_btn" type="submit" class="btn_full">Pay</button>
                            <a href="{{route('shop.cart.wechatCheckout')}}" id="wechat_submit_btn" class="btn_full" style=" display: none; background: #1b7e5a">Pay</a>
                            <a href="{{route('shop.cart.aliCheckout')}}" id="ali_submit_btn"  class="btn_full" style="display: none; background: #00adef">Pay</a>


                        </div><!-- End cart_box -->
                    </div><!-- End theiaStickySidebar -->
                </div><!-- End col-md-3 -->
            {!! Form::close() !!}
        {{--</form>--}}
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->




@endsection

@section('scripts')
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
    <script src="https://js.stripe.com/v2/"></script>
    <script src="{{asset('js/stripe/checkout.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('input:radio[name="paymentMethod"]').change(function () {
                var choice = $('input:radio[name="paymentMethod"]:checked').val();

                var credit_form_closed = false;

                if (choice == 1) {
                    $('#credit_submit_btn').css('display','block');
                    $('#wechat_submit_btn').css('display','none');
                    $('#ali_submit_btn').css('display','none');

                    $('#credit_card_form').show("fade",'slow');

                } else if (choice == 2) {
                    $('#wechat_submit_btn').css('display','block');
                    $('#credit_submit_btn').css('display','none');
                    $('#ali_submit_btn').css('display','none');

                    if($('#credit_card_form').is(":visible")){
                        $('#credit_card_form').hide("fade",'slow');
                    }
                }else if (choice == 3) {
                    $('#ali_submit_btn').css('display','block');
                    $('#credit_submit_btn').css('display','none');
                    $('#wechat_submit_btn').css('display','none');

                    if($('#credit_card_form').is(":visible")){
                        $('#credit_card_form').hide("fade",'slow');
                    }

                }
            })
        });


    </script>
@stop


