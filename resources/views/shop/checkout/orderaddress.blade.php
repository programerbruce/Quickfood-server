@extends('shop.layouts.index')

@section('css')

@stop

@section('sub_header')
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Fill out details</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#0" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_2.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_3.html" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
@stop


@section('content')
    <div class="container margin_60_35">
        <div class="row">
            @include('includes.form_error')
        </div>

        <div class="alert alert-danger {{ !Session::has('details_empty') ? 'hidden' : '' }}" role="alert">
            {{ Session::has('details_empty') ? session('details_empty') : ''  }}
        </div>

        <div class="row">

            {{--<Form id="delivery_form" action="{{route('shop.cart.goToCheckout')}}" method="post">--}}
            {!! Form::open(['method'=>'POST','action'=>'CartController@goToCheckout']) !!}


            <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />

            <div class="col-md-3">

                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                </div><!-- End box_style_1 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>

            </div><!-- End col-md-3 -->

            <!-- Delivery-Form -->
            <div class="col-md-6">
                <div class="box_style_2" id="order_process">
                    <h2 class="inner">Your order details</h2>
                    <div class="form-group">
                        <label>First name</label>
                        <input type="text" class="form-control" id="firstname_order" name="firstname_order" placeholder="First name">
                    </div>
                    <div class="form-group">
                        <label>Last name</label>
                        <input type="text" class="form-control" id="lastname_order" name="lastname_order" placeholder="Last name">
                    </div>
                    <div class="form-group">
                        <label>Telephone/mobile</label>
                        <input type="text" id="tel_order" name="tel_order" class="form-control" placeholder="Telephone/mobile">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" id="email_booking_2" name="email_order" class="form-control" placeholder="Your email">
                    </div>
                    <div class="form-group">
                        <label>Your full address</label>
                        <input type="text" id="address_order" name="address_order" class="form-control" placeholder=" Your full address">
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" id="city_order" name="city_order" class="form-control" placeholder="Your city">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Postal code</label>
                                <input type="text" id="pcode_oder" name="pcode_oder" class="form-control" placeholder=" Your postal code">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label>Time for {{session('cart')->choice ==1 ? 'Delivery' : 'Takeaway'}}</label>
                        </div>

                    </div>

                    <div style="margin: 10px" class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-check form-check-inline">
                                <font>Now</font>
                                <input class="form-check-input" type="radio" checked name="timeOption" value="1">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-check form-check-inline">
                                <font>Other time</font>
                                <input class="form-check-input" type="radio" name="timeOption" value="2">
                            </div>
                        </div>

                    </div>

                    <div id="time_container" style="display: none; margin-top: 20px" class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Delivery Day</label>
                                <select class="form-control" name="delivery_schedule_day" id="delivery_schedule_day">
                                    <option value="" selected>Select day</option>
                                    <option value="Today">Today</option>
                                    <option value="Tomorrow">Tomorrow</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Delivery time</label>
                                <select class="form-control" name="delivery_schedule_time" id="delivery_schedule_time">
                                    <option value="" selected>Select time</option>
                                    <option value="11.30am">11.30am</option>
                                    <option value="11.45am">11.45am</option>
                                    <option value="12.15am">12.15am</option>
                                    <option value="12.30am">12.30am</option>
                                    <option value="12.45am">12.45am</option>
                                    <option value="01.00pm">01.00pm</option>
                                    <option value="01.15pm">01.15pm</option>
                                    <option value="01.30pm">01.30pm</option>
                                    <option value="01.45pm">01.45pm</option>
                                    <option value="02.00pm">02.00pm</option>
                                    <option value="07.00pm">07.00pm</option>
                                    <option value="07.15pm">07.15pm</option>
                                    <option value="07.30pm">07.30pm</option>
                                    <option value="07.45pm">07.45pm</option>
                                    <option value="08.00pm">08.00pm</option>
                                    <option value="08.15pm">08.15pm</option>
                                    <option value="08.30pm">08.30pm</option>
                                    <option value="08.45pm">08.45pm</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr>


                    <div class="row">
                        <div class="col-md-12">

                            <label>Notes for the restaurant</label>
                            <textarea class="form-control" style="height:150px" placeholder="Ex. Allergies, cash change..." name="notes" id="notes"></textarea>

                        </div>
                    </div>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->
            <!-- End Delivery-Form -->


            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box" >
                        <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                        @if(Session::has('cart'))
                            <table class="table table_summary">
                                @if(count(session('cart')->items) > 0)
                                    <tbody>
                                    @foreach(session('cart')->items as $item)
                                        <tr>
                                            <td>
                                                 <strong>{{$item['qty']}}x</strong> {{$item['item']->name}}
                                            </td>
                                            <td>
                                                <strong class="pull-right">$ {{$item['price']}}</strong>
                                            </td>
                                        </tr>

                                    </tbody>
                                    @endforeach
                                @endif
                            </table>
                        @endif
                        <hr>
                        @if(Session::has('cart'))
                            <div class="row" id="options_2">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><span>{{Session::get('cart')->choice == '1'? 'Delivery' : 'Take Away'}}</span></label>
                                </div>

                            </div><!-- Edn options 2 -->
                        @else
                            <div class="row" id="options_2">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="1" checked name="option_2" class="form-check-input"><span style="padding: 10px">Delivery</span></label>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="2" name="option_2" class="form-check-input"><span style="padding: 10px">Take Away</span></label>
                                </div>
                            </div><!-- Edn options 2 -->
                        @endif
                        <hr>
                        <table class="table table_summary">
                            <tbody>
                            @if(Session::get('cart')->choice == '1')
                                <tr>
                                    <td>
                                        Delivery fee <span class="pull-right">$10</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td class="total">
                                    TOTAL <span class="pull-right">$ {{ Session::get('cart')->choice == '1' ?  session('cart')->totalPrice + 10 :  session('cart')->totalPrice }}</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                        @if(Session::has('cart'))
                            <a onclick="$(this).closest('form').submit()" class="btn_full">Order now</a>
                        @endif

                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

            </Form>
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection


@section('scripts')

    <script>
        $(document).ready(function() {
            $('input:radio[name="timeOption"]').click(function () {
                if ('$(input:radio[name="timeOption"]' == 2) {
                    $('#time_container').slideToggle('slow');
                } else {
                    $('#time_container').slideToggle('slow');
                }
            })
        });
    </script>

@stop