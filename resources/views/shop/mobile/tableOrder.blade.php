<!doctype html>
<html >
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta name="x5-fullscreen" content="true">
    <meta name="full-screen" content="yes">

    <link href="{{asset('css/mobile/tableorder/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/font/iconfont.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/style.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/common.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/mobile/tableorder/jquery-1.9.1.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('js/mobile/tableorder/layer_mobile/layer.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/mobile/tableorder/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/mobile/tableorder/common.js')}}" type="text/javascript"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/TouchSlide.1.1.js')}}"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/jquery.cookie.js')}}"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/jquery.nicescroll.js')}}"></script>
    <title>首页</title>
</head>
<body class="background-w">
<!--herder top-->
<header class="herder-style clearfix">
    <!--navbar-->
    <div class="herder-top">
        <div class="return"><a href="{{route('selectTable')}}">选桌</a></div>
        <div style="margin-left: 40px" class="title_name align">桌号{{$data['tableNo']}} </div>
        <div class="herder-operating">
            {{--<div class="Language-style dropdown">--}}
                {{--<a  class="relative clearfix" href="#" id="Language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<span class="name" ><i class="iconfont icon-duoyuyan"></i><b>Language</b></span>--}}
                    {{--<!--<i class="iconfont icon-jiantouarrow492 jiantou zk"></i>--}}
             {{--<i class="iconfont icon-jiantouarrow486 jiantou yc"></i>-->--}}
                {{--</a>--}}
                {{--<div class="dropdown-menu Language-select" aria-labelledby="dLabel" data-thumb-target="temp">--}}
                    {{--<h4 class="align padding5">Please select a language.</h4>--}}
                    {{--<ul class="language_list">--}}
                        {{--<li><a href="#"><i class="iconfont icon-icon-test"></i>日本語</a></li>--}}
                        {{--<li><a href="#">中文简</a></li>--}}
                        {{--<li><a href="#">中文繁</a></li>--}}
                        {{--<li><a href="#">English</a></li>--}}
                        {{--<li><a href="#">한국의</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="operating-style dropdown"  >--}}
                {{--<a class="iconfont icon-101 caozuo"  href="#" id="caozuo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>--}}
                {{--<ul class="dropdown-menu" aria-labelledby="dLabel" data-thumb-target="temp">--}}
                    {{--<li><a href="javascript:" onclick="share()"><i class="iconfont icon-fenxiang2"></i>シェア</a></li>--}}
                    {{--<li><a href="#"><i class="iconfont icon-setting"></i>設定</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
    <!--menu-->
    <div class="herder-menu">
        <ul>
            <li class="home active"><a href="{{route('tableOrder',['tableNo' => $data['tableNo']])}}" class="relative"><i class="iconfont icon-shop2"></i><span class="name">菜单</span></a></li>
            <li class="order"><a href="{{route('tableOrderReview',['tableNo' => $data['tableNo']])}}"><i class="iconfont icon-dingdan"></i><span class="name">桌单</span></a></li>
            {{--<li class="cart"><a href="cart.html" class="relative"><i class="iconfont icon-gouwuche1"><b class="cart-mark"></b></i><span class="name">购物车</span></a></li>--}}
            {{--<li class="order"><a href="Order.html"><i class="iconfont icon-dingdan"></i><span class="name">订单</span></a></li>--}}
            {{--<li class="pay"><a href="javascript:void(0)" onClick="Paybtn('Pay.html')"><i class="iconfont icon-yinxingqia-copy"></i><span class="name">结算</span></a></li>--}}
            {{--<li class="user"><a href="javascript:void(0)" onClick="qita()"><i class="iconfont icon-yonghu"></i><span class="name">用户</span></a></li>--}}
        </ul>
    </div>
</header>
<!--Content area-->
<input id="tableNo" type="hidden" value="{{$data['tableNo']}}">
<section class="container background-w">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="Stores-info box">
        <div class="Stores-img box-flex4"><div class="Stores-frame"><img src="{{asset('images/mobile/tableOrder/cz.jpg')}}" width="100%"></div></div>
        <div class="box-flex6 padding5 Introduce">
            <h4>店铺名称</h4>
            <h5>店铺信息介绍店铺信息介绍店铺信息介绍店铺信息介绍店铺信息介绍店铺信息介绍</h5>
        </div>
    </div>
    <!--Left column area-->
    <div class="switch_btn" id="switch_btn">类别</div>
    <div class="column-style" id="column-style">
        <ul class="column-list">
            <li class="column-level">
                <ul class="column-Lower">
                    <li><a href="#" class="column-name">名称</a></li>
                    <ul class="column-Lower">
                        @foreach($data['categories'] as $category)
                            <li><a href="#" class="column-name">{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </ul>
            </li>

        </ul>
    </div>
    <!--Right product list-->
    <div id="menu_list" class="product-style">
        <ul class="product-list padding5 clearfix">
            @foreach($data['menus'] as $menu)
                <li class="clearfix relative product_name">
                    <a href="javascript:void()" class="product-img" onClick="product_detailed({{$menu->id}})"><img src="{{asset($menu->photo)}}" alt=""></a>
                    <div class="product-xinxi">
                        <div class="title-name"><a href="javascript:void()" class="" onClick="product_detailed({{$menu->id}})">{{$menu->name}}</a></div>

                        <div class="operating">
                            <span class="price">$<b>{{$menu->price}}</b></span>
                            <div class="Quantity">
                                <button class="minus" style="display: inline-block;">
                                    <em class="iconfont icon-jian"></em>
                                </button>
                                <input id="qty-{{$menu->id}}" type="text" class="result" value="0" disabled="false">
                                <button class="add">
                                    <em class="iconfont icon-jia"></em>
                                </button>
                                <button onClick='addToOrder({{$menu->id}})' style="background: dodgerblue;height: 30px;width: 30px" class="btn btn-primary pull-right">添加</button>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</section>
</body>
</html>
<script>

    function addItem(menuId){
        $('#qty-'+menuId).val(parseInt($('#qty-'+menuId).val()) + 1)
    }

    function removeItem(menuId){
        if($('#qty-'+menuId).val() > 0) {
            $('#qty-' + menuId).val(parseInt($('#qty-' + menuId).val()) - 1);
        }
    }

    function addToOrder(menuId){
        var tableNo = $('#tableNo').val();
        var qty = $('#qty-'+menuId).val();

        if(qty == 0){
            alert('数量不能为0');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/ajaxAddMenuToTable',
            data: {'tableNo': tableNo,'menuId': menuId,'qty': qty},
            dataType: "json",
            success: function (data) {
                $('#qty-'+menuId).val(0);
            }
        });
    }



    //----------------------------------
    $(function(){
        //Set up plus or minus shopping cart into
        $(".add").click(function(){
            var totalBtn1 = 0
            var t=$(this).parent().find('input[class*=result]');
            t.val(parseInt(t.val())+1);
            if(parseInt(t.val())>0){
                $(this).parent().find("em").css("color","#000000");
            }

            setTotal();
        });

        $(".minus").click(function(){
            var t=$(this).parent().find('input[class*=result]');
            t.val(parseInt(t.val())-1);
            if(parseInt(t.val())<=0){
                t.val(0);
                $(this).find("em").css("color","#b9b9b9");
            }
            setTotal();
        })
        function setTotal(){
            var s=0;
            $(".Quantity").each(function(){
                s+=parseInt($(this).find('input[class*=result]').val());
            });
            if(s>0){
                $(".cart-mark").show();
            }else{
                $(".cart-mark").hide();
            }
        }
        setTotal();
    });
    //Adaptive style attributes
    function heightvalue(){
        $('.switch_btn').css('margin-top',-$("#switch_btn").height()/2);
        $('.column-style').css({'height':$(window).height()-$(".herder-style").height(),"top":$(".herder-style").height()});
        $('.product-style').css('height',$(window).height()-$(".herder-style").height());
    }
    heightvalue()
    $(window).resize(function(){
        heightvalue()
    })
    //
    $(window).scroll(function(){
        if($(window).scrollTop() >= $(".herder-style").height()){
            $('.column-style').css('position','fixed');
            $('.product-style').css('margin-left',$('.column-style').width());
        }else {
            $('.column-style').css('position','');
            $('.product-style').css('margin-left','');
        };
    });



    $('.column-Lower li').on('click',function(){
        var categoryName = $(this).children().html();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/ajaxRenderMenus',
            data: {"categoryName": categoryName},
            dataType: "json",
            success: function (data) {
                html ='<ul class="product-list padding5 clearfix">';
                var menus = data;
                for (i = 0; i < menus.length; i +=1) {
                    var menu = menus[i];

                    var menu_id = menu.id;

                    html += '<li class="clearfix relative product_name">\n' +
                        '            <a href="javascript:void()" class="product-img" onClick="product_detailed(' + menu_id + ')"><img src="http://localhost/deliverylog/public/' + menu.photo + '" alt=""></a>\n' +
                        '            <div class="product-xinxi">\n' +
                        '                <div class="title-name"><a href="javascript:void()" class="" onClick="product_detailed(' + menu_id + ')">' + menu.name + '</a></div>\n' +
                        '\n' +
                        '                <div class="operating">\n' +
                        '                    <span class="price">$<b>' + menu.price + '</b></span>\n' +
                        '                    <div class="Quantity">\n' +
                        '                        <button onclick="removeItem(' + menu.id +')" class="minus" style="display: inline-block;">\n' +
                        '                            <em class="iconfont icon-jian"></em>\n' +
                        '                        </button>\n' +
                        '                    <input id="qty-' + menu.id +'" type="text" class="result" value="0" disabled="false">\n' +
                        '                        <button onclick="addItem(' + menu.id +')" class="add">\n' +
                        '                            <em class="iconfont icon-jia"></em>\n' +
                        '                        </button>\n' +
                        '                        <button onclick=\'addToOrder(' + menu.id + ')\' style="background: dodgerblue;height: 30px;width: 30px" class="btn btn-primary pull-right">添加</button>\n' +
                        '                    </div>\n' +
                        '\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '        </li>';
                }

                html += '</ul>';

                $('#menu_list').html(html);

            }
        });


        $(this).each(function(){
            $(this).addClass('active');
            $(this).siblings('li').removeClass('active');
            // $(this).parent().siblings('li').removeClass('active');
        },function(){
            // 删除其他兄弟元素的样式
            $(this).siblings('li').removeClass('active');
        })


        $("#column-style").animate({width: '0'},300);
        $("#switch_btn").animate({left: '0'},300);
        $("#switch_btn").css({'background-position':'0px 0',"opacity":"0.8","margin-top":-$("#switch_btn").height()/2});// 添加当前元素的样式


    })
    //Verification code

    function Prompt_click(){
        layer.open({
            title:['验证码', 'background-color: #f0f0f0; color:#333333; height:50px; line-height:50px; '],
            style: 'background-color:#ffffff; color:#333333; border:none; font-size:22px',
            content:'	<div class="prompt-content mb3"><input type="text" name="verification"  placeholder="验证码" class="verification-text" id="verification"/><h5 class="prompt_xinx"></h5></div><div class="button-operation btn-one clearfix"><a  href="#" onclick="user_Prompt()"  class="btn btn-confirm">确认</a></div>',
            success: function(layero,index){
                $('.layui-m-layerchild').append('<div class="share_close uertification_close" onclick="uertificationx()"><i class="iconfont icon-quxiao" style="color:#333333"></i></div>');
                $("#Prompt").css("display","block");
                if($.cookie("verification")!=null){$("#verification").val( $.cookie("123456")||$.cookie("111111"));	}
            }
        });
        Certification();
    }
    function uertificationx(event){
        layer.closeAll();
        $("#Prompt").css("display","block");
    }
    function Paybtn(url){
        layer.open({
            title:['確認', 'background-color: #f0f0f0; color:#333333; height:50px; line-height:50px; text-align:left'],
            style: 'background-color:#ffffff; color:#333333; border:none; font-size:20px;',
            content:'	<div class="prompt-content mb3">お会計でよろしいでしょうか？</h5></div><div class="button-operation clearfix"><a  href="'+url+'" onclick="layer.closeAll();"  class="btn btn-confirm">はい</a><a  href="javascript:" onclick="layer.closeAll();"  class="btn btn-close">いいえ</a></div>',

            success: function(layero,index){
                $('.layui-m-layerchild').append('<div class="share_close" onclick="layer.closeAll();"><i class="iconfont icon-quxiao" style="color:#333333"></i></div>');
            } ,
        });
    }
    function qita(){
        layer.open({
            content:'開通していない機能',
            style: 'background-color:#ffffff; color:#333333; border:none;',
            time: 3,
            success: function(){
                $(".layui-m-layercont").css({"padding":"20px"})
            }
        })
    }
    //----------------------------------

    //share
    function share(){
        layer.open({
            type: 1,
            title: ['シェア','background-color: #000000; color:#fff; border-radius:0px; height:50px; line-height:50px'],
            content:'<div class="share_style"><ul class="clearfix"><li><i class="iconfont icon-facebook2 "></i><h4>facebook</h4></li><li><i class="iconfont icon-twitter2"></i><h4>twitter</h4></li><li><i class="iconfont icon-big-WeChat"></i><h4>WeChat</h4></li></ul></div>',
            anim: 'up',
            style: 'position:fixed; bottom:0; left:0; width: 100%; height: 200px;  border:none;',
            success: function(layero,index){
                $('.layui-m-layerchild').append('<div class="share_close" onclick="layer.closeAll();"><i class="iconfont icon-quxiao"></i></div>');
            }
        });
    }
    //
    function product_detailed(menu_id){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/ajaxGetMenu/' + menu_id,
            data: '',
            dataType: "json",
            success: function (data) {
                var menu = data;
                layer.open({
                    type: 1,
                    shade: 0.5,
                    closeBtn: 0,
                    fix:false,
                    move:false,moveOut:true,
                    anim: 'up',
                    content:'',
                    style: 'position:fixed;left:0; width:100%;border: none; z-index:1111;    -webkit-animation-duration: .5s; animation-duration: .5s;overflow-y: scroll ',
                    //  shade: false,
                    success: function(layero,index){
                        var p_2_path = '{{ URL::asset('/images/mobile/tableOrder/p-2.jpg') }}';
                        var htmls="";
                        $('.layui-m-layerchild').append('<div class="detailed_close" onclick="layer.closeAll();"><i class="iconfont icon-quxiao"></i></div>');
                        htmls+='<div class="detailed" id="detailed"><div id="detailed_style">';
                        htmls+='<div id="focus" class="focus"><div class="hd"><ul></ul></div><div class="bd">' +
                            '<ul>' +
                            '<li><a href="#"><img _src="http://localhost/deliverylog/public/'+ menu.photo +'" src="http://localhost/deliverylog/public/images/mobile/tableOrder/blank.png" /></a></li>' +
                            '</ul>' +
                            '</div></div><script type="text/javascript">TouchSlide({ slideCell:"#focus",titCell:".hd ul", mainCell:".bd ul", effect:"left", autoPlay:false,autoPage:true, switchLoad:"_src" });<\/script>';//AD
                        htmls+='<div class="lable-style"></div><div class="title_name">'+ menu.name +'</div><div class="relative"><h3 class="price">$<b>'+ menu.price +'</b></h3><div class="Quantity"><button class="minus" style="display: inline-block;"><em class="iconfont icon-jianqu" style="font-size:26px"></em></button><input type="text" class="result" value="0" disabled="false"><button class="add"><em class="iconfont icon-jia1"></em> </button> </div></div>';
                        htmls+='<div class="Introduction"><h4 class="title_name">介绍<h4><div class="content">'+ menu.description +'<div><div>';
                        htmls+='</div></div>';
                        $(".layui-m-layercont").append(htmls);
                        $('#detailed').css('height',$(window).height()-$(".herder-style").height()-10);
                        Certification(); //Judgment verification;
                        $("#detailed").niceScroll({
                            cursorcolor:"#888888",
                            cursoropacitymax:1,
                            touchbehavior:false,
                            cursorwidth:"1px",
                            cursorborder:"0",
                            cursorborderradius:"1px"
                        });
                    }
                });
                $('.layui-m-anim-up').css({'height':$(window).height()-$(".herder-style").height(), 'top':$(".herder-style").height()});


            }
        });


    }


</script>
