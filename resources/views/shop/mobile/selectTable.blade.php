<!doctype html>
<html >
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta name="x5-fullscreen" content="true">
    <meta name="full-screen" content="yes">

    <link href="{{asset('css/mobile/tableorder/font/iconfont.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/style.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/common.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">

    <script src="{{asset('js/mobile/tableorder/jquery-1.9.1.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('js/mobile/tableorder/layer_mobile/layer.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/mobile/tableorder/common.js')}}" type="text/javascript"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/TouchSlide.1.1.js')}}"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/jquery.cookie.js')}}"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/jquery.nicescroll.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>首页</title>
</head>
<body class="background-w">
<!--herder top-->
<header class="herder-style clearfix">
    <!--navbar-->
    <div class="herder-top">
        {{--<div class="return"><a href="javascript :history.back(-1)"><i class="iconfont icon-fanhui"></i></a></div>--}}
        <div style="margin-left: 80px" class="title_name align">选取桌号</div>
        <div class="herder-operating">

        </div>
    </div>
    <!--menu-->
    <div class="herder-menu">
        <ul>
            <li class="order"><a href="#"><i class="iconfont icon-yonghu"></i><span class="name">选取点单桌号</span></a></li>
        </ul>
    </div>
</header>
<!--Content area-->
<input id="tableNo" type="hidden" value="0">
<section class="container">
    <form action="{{route('selectTableRedirect')}}" method="post">
    {!! csrf_field() !!}
    <div style="margin-top: 30px" class="row">
        <div class="col-xs-8">
            <select class="form-control" name="tableNo" id="tableNo">
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
            </select>
        </div>
        <div class="col-xs-4">
            <button type="submit" style="margin-left: 20px" class="btn btn-primary">确定</button>
        </div>
    </div>
    </form>


</section>
<!--Prompt box-->
<div id="Prompt" style="display: none">
    <!--<div class="Prompt-cover"></div>	-->

</div>
</body>
</html>
<script>

    $(".dropdown-menu li a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText);
    });

</script>
