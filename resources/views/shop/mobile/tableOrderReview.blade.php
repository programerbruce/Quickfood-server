<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
    <meta name="x5-fullscreen" content="true">
    <meta name="full-screen" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="{{asset('css/mobile/tableorder/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/font/iconfont.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/style.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('css/mobile/tableorder/common.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/mobile/tableorder/jquery-1.9.1.min.js')}}" type="text/javascript" ></script>
    <script src="{{asset('js/mobile/tableorder/layer_mobile/layer.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/mobile/tableorder/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/mobile/tableorder/common.js')}}" type="text/javascript"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/TouchSlide.1.1.js')}}"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/jquery.cookie.js')}}"></script>
    <script  type="text/javascript" src="{{asset('js/mobile/tableorder/jquery.nicescroll.js')}}"></script>
    <title>注文履歴</title>
</head>

<body>
<!--herder top-->
<header class="herder-style clearfix">
    <!--navbar-->
    <div class="herder-top">
        <div class="return"><a href="{{route('selectTable')}}">选桌</a></div>
        <div style="margin-left: 40px" class="title_name align">桌号{{$data['tableNo']}} </div>
        <div class="herder-operating">

        </div>
    </div>
    <!--menu-->
    <div class="herder-menu">
        <ul>
            <li class="home"><a href="{{route('tableOrder',['tableNo' => $data['tableNo']])}}" class="relative"><i class="iconfont icon-shop2"></i><span class="name">菜单</span></a></li>
            <li class="order active"><a href="{{route('tableOrderReview',['tableNo' => $data['tableNo']])}}"><i class="iconfont icon-dingdan"></i><span class="name">桌单</span></a></li>
            {{--<li class="cart"><a href="cart.html" class="relative"><i class="iconfont icon-gouwuche1"><b class="cart-mark"></b></i><span class="name">购物车</span></a></li>--}}
            {{--<li class="order"><a href="Order.html"><i class="iconfont icon-dingdan"></i><span class="name">订单</span></a></li>--}}
            {{--<li class="pay"><a href="javascript:void(0)" onClick="Paybtn('Pay.html')"><i class="iconfont icon-yinxingqia-copy"></i><span class="name">结算</span></a></li>--}}
            {{--<li class="user"><a href="javascript:void(0)" onClick="qita()"><i class="iconfont icon-yonghu"></i><span class="name">用户</span></a></li>--}}
        </ul>
    </div>
</header>
<!--Content area-->
<section class="container pages_style">
    <div class="cart-style">
        <table class="table order-list">
            <tbody>
            <tr class="cart_time"><td colspan="4">待处理点单</td></tr>
            @php ($total = 0)
            @foreach($data['menus'] as $menu)
            <tr>
                <td width="20%"><span class="name" onClick="product_name(this,'{{$menu->menu->name}}')">{{$menu->menu->name}}</span></td>
                <td width="10%"><span class="price">$<b class="amount">{{$menu->menu->price}}</b></span></td>
                <td width="10%"><span class="amount">x{{$menu->qty}}</span></td>
                <td width="20%">
                    <span class="Subtotal">$<b>{{$menu->qty * $menu->menu->price}}</b></span>
                </td>
                <td width="5%">
                    <form action="{{route('deleteOrderedItemOnce')}}" method="post">
                        {!! csrf_field() !!}

                        <input type="hidden" name="tableNo" value="{{$data['tableNo']}}">
                        <input type="hidden" name="tableOrderId" value="{{$menu->id}}">
                    <button type="submit" class="minus" style="display: inline-block;">
                        <em class="iconfont icon-jian"></em>
                    </button>
                    </form>
                </td>
                <td width="10%">
                    <form action="{{route('deleteOrderedItemAll')}}" method="post">
                        {!! csrf_field() !!}

                        <input type="hidden" name="tableNo" value="{{$data['tableNo']}}">
                        <input type="hidden" name="tableOrderId" value="{{$menu->id}}">
                    <span class="Subtotal"><button class="btn btn-danger">删除</button></span>
                    </form>
                </td>

            </tr>
            @php ($total += $menu->qty * $menu->menu->price)
            @endforeach
            <tr class="caet_Subtotal"><td colspan="3">总计：<b>${{$total}}</b></td>
                <td colspan="1"><button onclick="sendToKitchen()" class="btn btn-success">提交</button></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="cart-style">
        <table class="table order-list">
            <tbody>
            <tr class="cart_time"><td colspan="4">厨房已打印订单</td></tr>
            @php ($total = 0)
            @foreach($data['printed_menus'] as $menu)
                <tr>
                    <td width="50%"><span class="name" onClick="product_name(this,'{{$menu->menu->name}}')">{{$menu->menu->name}}</span></td>
                    <td width="20%"><span class="price">$<b class="amount">{{$menu->menu->price}}</b></span></td>
                    <td width="5%"><span class="amount">x{{$menu->qty}}</span></td>
                    <td width="25%">
                        <span class="Subtotal">$<b>{{$menu->qty * $menu->menu->price}}</b></span>
                    </td>
                </tr>
                @php ($total += $menu->qty * $menu->menu->price)
            @endforeach
            <tr class="caet_Subtotal"><td colspan="4">总计：<b>${{$total}}</b></td>
                {{--<td colspan="1"><button class="btn btn-success">提交</button></td>--}}
            </tr>
            </tbody>
        </table>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">请确认</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    确定把待处理点单送去厨房打印？
                </div>
                <form action="{{route('tableOrderSendToKitchen',['tableNo' => $data['tableNo']])}}" method="post">
                    {!! csrf_field() !!}
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                    <button onclick="proceedToKitchen()" type="submit" class="btn btn-primary">确定</button>
                </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!---->
{{--<div class="footer-style">--}}
    {{--<div class="Subtotal">合計:¥<b class="price">124,590</b>JPY</div>--}}
    {{--<button type="button" class="btn cart-btn width35" onClick="btn_accountant()">会計</button>--}}
{{--</div>--}}
</body>
</html>
<script>
    //

    function sendToKitchen(){
        $('#exampleModal').modal();
    }

    function proceedToKitchen(){
        $('#exampleModal').modal('hide');

    }

    function commodityWhole() {
        var univalent = $(".Subtotal .price").text();
        if (univalent == 0) {
            $(".cart-btn").css("background", "#b9b9b9")
            $(".cart-btn").attr('disabled', 'true');
            $(".cart-mark").hide();
            $(document).find('section').append("<div class='message-name'>お支払金額がありません。</div>")
        } else {
            $(".cart-btn").css("background", "#a08455");
            $(".cart-btn").removeAttr('disabled');
            $(".cart-mark").show();
            $(document).find('section .message-name').remove();
        }
    }
    commodityWhole();
    /**************************/
    function product_name(index,name){
        layer.open({
            content:name,
            style: 'background-color:#ffffff; color:#333333; border:none;',
            time: 3,
            success: function(){
                $(".layui-m-layercont").css({"padding":"20px"})
            }
        });
    }
    function qita(){
        layer.open({
            content:'開通していない機能',
            style: 'background-color:#ffffff; color:#333333; border:none;',
            time: 3,
            success: function(){
                $(".layui-m-layercont").css({"padding":"20px"})
            }
        })
    }
    //share
    function share(){
        layer.open({
            type: 1,
            title: ['シェア','background-color: #000000; color:#fff; border-radius:0px; height:50px; line-height:50px'],
            content:'<div class="share_style clearfix"><ul class="clearfix"><li><i class="iconfont icon-facebook2 "></i><h4>facebook</h4></li><li><i class="iconfont icon-twitter2"></i><h4>twitter</h4></li><li><i class="iconfont icon-big-WeChat"></i><h4>WeChat</h4></li></ul></div>',
            anim: 'up',
            style: 'position:fixed; bottom:0; left:0; width: 100%; height: 200px;  border:none;',
            success: function(layero,index){
                $('.layui-m-layerchild').append('<div class="share_close" onclick="layer.closeAll();"><i class="iconfont icon-quxiao"></i></div>');
            }
        });
    }
    //
    /********accountant prompt*******/
    function btn_accountant(){
        layer.open({
            title:['確認', 'background-color: #f0f0f0; color:#333333; height:50px; line-height:50px; text-align:left'],
            style: 'background-color:#ffffff; color:#333333; border:none; font-size:20px',
            content:'	<div class="prompt-content mb3 ">お会計でよろしいでしょうか？</div><div class="button-operation clearfix"><a  href="prompt-wait.html" onclick="layer.closeAll();"  class="btn btn-confirm">はい</a>  <a  href="javascript:" onclick="layer.closeAll();"  class="btn btn-close">いいえ</a></div>',
            success: function(layero,index){
                $('.layui-m-layerchild').append('<div class="share_close" onclick="layer.closeAll();"><i class="iconfont icon-quxiao" style="color:#333333"></i></div>');

            }
        });

    }
    function Paybtn(url){
        layer.open({
            title:['確認', 'background-color: #f0f0f0; color:#333333; height:50px; line-height:50px; text-align:left'],
            style: 'background-color:#ffffff; color:#333333; border:none; font-size:20px;',
            content:'	<div class="prompt-content mb3">お会計でよろしいでしょうか？</h5></div><div class="button-operation clearfix"><a  href="'+url+'" onclick="layer.closeAll();"  class="btn btn-confirm">はい</a><a  href="javascript:" onclick="layer.closeAll();"  class="btn btn-close">いいえ</a></div>',

            success: function(layero,index){
                $('.layui-m-layerchild').append('<div class="share_close" onclick="layer.closeAll();"><i class="iconfont icon-quxiao" style="color:#333333"></i></div>');
            } ,
        });
    }
</script>
