@extends('shop.layouts.index')


@section('sub_header')
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Your orders</h1>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@stop

@section('content')
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
            @if(count($orders) > 0)

                @for ($i = 0; $i < count($orders); $i += 3)

                    @php ($order_one = null)
                    @php ($order_two = null)
                    @php ($order_three = null)

                    @if ($orders[$i])
                        @php ($order_one = $orders[$i])
                    @endif

                    @if(($i + 1) < count($orders))
                        @php($order_two = $orders[$i + 1])
                    @endif

                    @if(($i + 2) < count($orders))
                        @php($order_three = $orders[$i + 2])
                    @endif
                    <div class="row">
                        @if($order_one != null)
                            <div class="col-md-4">
                                <div class="box_style_2">
                                   <h2 class="inner">Order no:  <font style="font-size: 15px"> {{$order_one->payment_id}} </font></h2>
                                    <h2 class="inner">{{$order_one->created_at}}</h2>
                                    <div id="confirm">
                                    </div>
                                    <h4>Summary</h4>
                                    <table class="table table-striped nomargin">
                                        <thead>
                                        <tr>
                                            <th>Picture</th>
                                            <th>Dish</th>
                                            <th>QTY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order_one->cart->items as $item)
                                            <tr>
                                                <td>
						    @php ($menu = \App\Menu::findOrFail($item['item']->id))
                                                    <strong><img style="height: 50px" src="{{asset($menu->photo)}}" alt=""></strong>
                                                </td>
                                                <td>
                                                    <strong>{{$item['item']['name']}}</strong>
                                                </td>
                                                <td>
                                                    <strong class="pull-right">{{$item['qty']}} </strong>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <br>
                                        <h4><lable>Total: $</lable>{{$order_one->amount}}</h4>
                                    <br>
			            @if($order_one->cart->addressDetail['Note'] != null)
                                        <h4><font color="red">Note:</font></h4>
                                        <p>{{$order_one->cart->addressDetail['Note']}}</p>
                                    @endif
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <label>Order status</label>
                                                </td>
                                                <td>
                                                    <strong style="color: #20c997">
                                                    @if($order_one->status == 1)
                                                        Order confirmed
                                                    @elseif($order_one->status == 2)
                                                        Order preparing
                                                    @elseif($order_one->status == 3)
                                                        Order on delivery
                                                    @elseif($order_one->status == 4)
                                                        Order finished
                                                    @endif
                                                    </strong>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                        @endif
                        @if($order_two != null)
                            <div class="col-md-4">
                                <div class="box_style_2">
                                    <h2 class="inner">Order no:  <font style="font-size: 15px"> {{$order_two->payment_id}} </font></h2>
                                    <h2 class="inner">{{$order_two->created_at}}</h2>
                                    <div id="confirm">
                                    </div>
                                    <h4>Summary</h4>
                                    <table class="table table-striped nomargin">
                                        <thead>
                                        <tr>
                                            <th>Picture</th>
                                            <th>Dish</th>
                                            <th>QTY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order_two->cart->items as $item)
                                            <tr>
                                                <td>
						   @php ($menu = \App\Menu::findOrFail($item['item']->id))
                                                    <strong><img style="height: 50px" src="{{asset($menu->photo)}}" alt=""></strong>
                                                </td>
                                                <td>
                                                    <strong>{{$item['item']['name']}}</strong>
                                                </td>
                                                <td>
                                                    <strong class="pull-right">{{$item['qty']}} </strong>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
				    <br>
                                        <h4><lable>Total: $</lable>{{$order_two->amount}}</h4>
                                    <br>
                                    @if($order_two->cart->addressDetail['Note'] != null)
                                        <h4><font color="red">Note:</font></h4>
                                        <p>{{$order_two->cart->addressDetail['Note']}}</p>
                                    @endif
                                    <form id="update_form2" style="margin-top: 20px" action="{{route("admin.order.management.update")}}" method="post">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <label>Order status</label>
                                                </td>
                                                <td>
                                                    <strong style="color: #20c997">
                                                    @if($order_two->status == 1)
                                                        Order confirmed
                                                    @elseif($order_two->status == 2)
                                                        Order preparing
                                                    @elseif($order_two->status == 3)
                                                        Order on delivery
                                                    @elseif($order_two->status == 4)
                                                        Order finished
                                                    @endif
                                                    </strong>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>

                                </div>
                            </div>
                        @endif

                        @if($order_three != null)
                            <div class="col-md-4">
                                <div class="box_style_2">
                                    <h2 class="inner">Order no:  <font style="font-size: 15px"> {{$order_three->payment_id}} </font></h2>
                                    <h2 class="inner">{{$order_three->created_at}}</h2>
                                    <div id="confirm">
                                    </div>
                                    <h4>Summary</h4>
                                    <table class="table table-striped nomargin">
                                        <thead>
                                        <tr>
                                            <th>Picture</th>
                                            <th>Dish</th>
                                            <th>QTY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order_three->cart->items as $item)
                                            <tr>
                                                <td>
						    @php ($menu = \App\Menu::findOrFail($item['item']->id))
                                                    <strong><img style="height: 50px" src="{{asset($menu->photo)}}" alt=""></strong>
                                                </td>
                                                <td>
                                                    <strong>{{$item['item']['name']}}</strong>
                                                </td>
                                                <td>
                                                    <strong class="pull-right">{{$item['qty']}} </strong>
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
				    <br>
                                   	 <h4><lable>Total: $</lable>{{$order_three->amount}}</h4>
                                    <br>
                                    @if($order_three->cart->addressDetail['Note'] != null)
                                        <h4><font color="red">Note:</font></h4>
                                        <p>{{$order_three->cart->addressDetail['Note']}}</p>
                                    @endif
                                    <form id="update_form3" style="margin-top: 20px" action="{{route("admin.order.management.update")}}" method="post">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <label>Order status</label>
                                                </td>
                                                <td>
                                                    <strong style="color: #20c997">
                                                    @if($order_three->status == 1)
                                                        Order confirmed
                                                    @elseif($order_three->status == 2)
                                                        Order preparing
                                                    @elseif($order_three->status == 3)
                                                        Order on delivery
                                                    @elseif($order_three->status == 4)
                                                        Order finished
                                                    @endif
                                                    </strong>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        @endif

                    </div>

                @endfor
                <div class="text-center">{{ $orders->links() }}</div>

                <!-- Modal -->
                <div id="alert_modal" class="modal fadeIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Status Notice</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Selected update status cannot be null. Please pick a status before press update button.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <p class="center">Recently, had no orders in our system.</p>
            @endif

    </div><!-- End container -->
    <!-- End Content =============================================== -->
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v2/"></script>
    <script src="{{asset('js/stripe/checkout.js')}}"></script>
@stop


