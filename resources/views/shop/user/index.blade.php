@extends('shop.layouts.index')

@section('css')

    <!-- Font awesome CSS -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{asset('css/style-329.css')}}" rel="stylesheet">

@stop

@section('sub_header')
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset('img/chicken_header.png')}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>User Profile</h1>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
@stop


@section('content')
    @if(session('status_info'))
        <div class="alert alert-info" role="alert">
             <strong>{{session('status_info')}}</strong>
        </div>
    @endif
    <!-- UI # -->
    <div class="ui-329">

        <div  class="container">
            <div class="row">
                <div class="row">
                    <!-- Item -->
                    <div class="item">
                        <!-- Head -->
                        <div class="head clearfix">
                            <!-- Image -->
                            {!! Auth::user()->photo ? html_entity_decode('<img class="img-responsive" src=' . asset(Auth::user()->photo->file)) . '>' : '<img class="img-responsive" src='. asset("img/unlogged_user.png") .'>' !!}
                            <!-- Name and designation -->
                            <h2><a href="#"> {{Auth::user()->name}}</a> <span>{{Auth::user()->role->name}}</span></h2>
                            <i class="fa fa-envelope"></i> {{Auth::user()->email != null ? Auth::user()->email : 'None'}}

                        </div>
                        <!-- Content -->
                        <div class="content">
                            <ul class="list-unstyled">
                                <!-- Icon with content -->
                                <li>
                                    <table>
                                        <tr>
                                            <td>
                                                <i class="fa fa-picture-o bg-purple"></i>
                                            </td>
                                            {!! Form::open(['method'=>'POST','action'=>'UserController@updatePhoto','files' => true]) !!}
                                            <td>
                                                {!! Form::file('photo',null,['class'=>'form-control']) !!}                                            </td>
                                            <td>
                                                <div style="margin-left: 20px">
                                                    {!! Form::submit('Update Photo', ['class'=>'btn btn-primary']) !!}
                                                </div>
                                            </td>
                                            {!! Form::close() !!}
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tr>
                                            <td>
                                                <i class="fa fa-user bg-lblue"></i>
                                            </td>
                                            {!! Form::open(['method'=>'POST','action'=>'UserController@updateName']) !!}
                                            <td>
                                                {!! Form::text('name',Auth::user()->name != null ? Auth::user()->name : '',array('placeholder' => 'Type your name', 'size'=> 30),['class'=>'form-control']) !!}
                                            </td>
                                            <td>
                                                <div style="margin-left: 20px">
                                                    {!! Form::submit('Update Name', ['class'=>'btn btn-primary']) !!}
                                                </div>
                                            </td>
                                            {!! Form::close() !!}
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tr>
                                            <td>
                                                <i class="fa fa-phone bg-red"></i>
                                            </td>
                                            {!! Form::open(['method'=>'POST','action'=>'UserController@updatePhone']) !!}
                                            <td>
                                                {!! Form::text('phone',Auth::user()->phone != null ? Auth::user()->phone : '',array('placeholder' => 'Type your phone', 'size'=> 30),['class'=>'form-control']) !!}
                                            </td>
                                            <td>
                                                <div style="margin-left: 20px">
                                                    {!! Form::submit('Update Phone', ['class'=>'btn btn-success']) !!}
                                                </div>
                                            </td>
                                        {!! Form::close() !!}
                                        </tr>
                                    </table>
                                </li>
                                {{--<li>--}}
                                    {{--<table>--}}
                                        {{--<tr>--}}
                                            {{--<td>--}}
                                                {{--<i class="fa fa-key bg-green"></i>--}}
                                            {{--</td>--}}
                                            {{--{!! Form::open(['method'=>'POST','action'=>'UserController@changePassword']) !!}--}}
                                            {{--<td>--}}
                                                {{--{!! Form::password('password',null,array('placeholder' => 'Type a new password', 'size'=> 30),['class'=>'form-control']) !!}--}}
                                            {{--</td>--}}
                                            {{--<td>--}}
                                                {{--<div style="margin-left: 20px">--}}
                                                    {{--{!! Form::submit('Change password', ['class'=>'btn btn-danger']) !!}--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                            {{--{!! Form::close() !!}--}}
                                        {{--</tr>--}}
                                    {{--</table>--}}
                                {{--</li>--}}
                                {{--<li><a href="#"><i class="fa fa-twitter bg-lblue"></i> @janedoe</a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-facebook bg-blue"></i> facebook.com/janedeo</a></li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('scripts')
    <!-- Javascript files -->
    <!-- jQuery -->
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Placeholder JS -->
    <script src="js/placeholder.js"></script>
    <!-- Respond JS for IE8 -->
    <script src="js/respond.min.js"></script>
    <!-- HTML5 Support for IE -->
    <script src="js/html5shiv.js"></script>
@stop



