Stripe.setPublishableKey('pk_test_cbpdi1fF4V3qhwD91WVE18Y9');

var $form = $('#checkout-form');

$form.submit(function(event){
    $('#charge_error').addClass('hidden');
    $('#submit_btn').html('Processing');
    $('#submit_btn').prop('disabled', true);
    Stripe.card.createToken({
        name: $('#name_card_order').val(),
        number: $('#card_number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('#expire_month').val(),
        exp_year: $('#expire_year').val()
    }, stripeResponseHandler);

    return false;
});

function stripeResponseHandler(status, response){
    if(response.error){
        $('#charge_error').removeClass('hidden');
        $('#charge_error').text(response.error.message);
        $('#submit_btn').html('Pay');
        $('#submit_btn').prop('disabled', false);
    }else{
        var token = response.id;

        // Insert the token into the form so it gets submitted to the server:
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));

        // Submit the form:
        $form.get(0).submit();
    }
}

