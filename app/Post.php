<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    protected $fillable = [

        'user_id',
        'category_id',
        'photo_id',
        'title',
        'body',
        'created_at',
        'updated_at',


    ];



    public function user(){

        return $this->belongsTo('App\User');
    }

    public function category(){

        return $this->belongsTo('App\Category');
    }

    public function photos(){

        return $this->hasMany('App\Photo', 'id', 'photo_id');
    }

    public function comments(){

        return $this->hasMany('App\Comment', 'post_id', 'id');
    }
}
