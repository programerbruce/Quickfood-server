<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2018/4/9
 * Time: 上午10:33
 */

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class Cart{

    public $items;
    public $choice = 1;
    public  $totalQty = 0;
    public $totalPrice = 0;
    public $addressDetail;

    public function __construct($oldCart, $choice){

        if($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
            $this->choice = $choice;
            $this->addressDetail = $oldCart->addressDetail;
        }else{

            $this->choice = $choice;
        }

    }


    public function setAddressDetail(
        $firstName,$lastName,$phone,$email,$address,$city,$postCode,$desireTime,$Note){

        $addressDetail = [
            'firstName' => $firstName,
            'lastName' => $lastName,
            'phone' => $phone,
            'email' => $email,
            'address' => $address,
            'city' => $city,
            'postCode' => $postCode,
            'desireTime' => $desireTime,
            'Note' => $Note,
            ];

        $this->addressDetail = $addressDetail;

    }

    /**
     * @param mixed $choice
     */
    public function setChoice($choice)
    {
        $this->choice = $choice;
    }

    /**
     * @return mixed
     */
    public function getChoice()
    {
        return $this->choice;
    }

    public function add($item, $id, $qty){
        $storedItem = ['id'=> $id,'item' => $item, 'price' => $item->price, 'qty' => 0];

        if($this->items){
            if(array_key_exists($item->id, $this->items)){
                $storedItem = $this->items[$id];

            }
        }

        $storedItem['qty'] += $qty;

//        $storedItem['price'] =  round($item->price * $storedItem['qty'],2);


        $storedItem['price'] = number_format((float)$item->price * $storedItem['qty'], 2, '.', '');

        $this->items[$id] = $storedItem;

        $this->totalQty++;

        $this->totalPrice = number_format($item->price + $this->totalPrice, '2','.','');

    }


    public function minus($item, $id, $qty)
    {

        if($this->items){
            if(array_key_exists($item->id, $this->items)){
                $storedItem = $this->items[$id];

            }
        }

        $storedItem['qty'] -= $qty;

//        $storedItem['price'] =  round($item->price * $storedItem['qty'],2);


        $storedItem['price'] = number_format((float)$item->price * $storedItem['qty'], 2, '.', '');

        $this->items[$id] = $storedItem;

        $this->totalQty--;

        $this->totalPrice = number_format($this->totalPrice - $item->price, '2','.','');

        if($storedItem['qty'] == 0){
            unset($this->items[$id]);
        }
    }


}