<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentReply extends Model
{
    //

    protected $fillable = [

        'comment_id',
        'author',
        'body',
        'is_active',
        'created_at',
        'updated_at',

    ];


    public function get_author(){

        return $this->belongsTo('App\User','author','id');
    }
}
