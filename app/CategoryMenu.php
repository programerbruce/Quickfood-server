<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryMenu extends Model
{
    //

    protected $fillable = [

        'id',
        'name',
    ];

    public function menus(){

        return $this->hasMany('App\Menu','category_menus_id','id');
    }
}
