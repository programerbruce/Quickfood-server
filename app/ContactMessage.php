<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMessage extends Model
{
    //
    protected $fillable = [

        'id',
        'name',
        'phone',
        'email',
        'message',
        'status',
    ];
}
