<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableOrder extends Model
{
    //
    protected $fillable = [
        'id',
        'tableNo',
        'menu_id',
        'qty',
        'status',
    ];

    public function menu(){

        return $this->belongsTo('App\Menu','menu_id','id');
    }
}
