<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $fillable = [
        'id',
        'category_menus_id',
        'name',
        'description',
        'price',
        'photo',
        'created_at',
        'updated_at',
    ];

    public function category(){

        return $this->belongsTo('App\CategoryMenu','category_menus_id','id');
    }
}
