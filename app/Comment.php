<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    protected $fillable = [

        'author',
        'post_id',
        'body',
        'is_active',

    ];

    public function user(){

        return $this->belongsTo('App\User','author','id');
    }

    public function post(){

        return $this->belongsTo('App\Post','post_id','id');
    }

    public function replies(){

        return $this->hasMany('App\CommentReply','comment_id','id');
    }
}
