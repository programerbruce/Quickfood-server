<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $status_info = '';

        if(Auth::check()){

            if(Auth::user()->isActive()) {

                return $next($request);

            }else{
                $status_info = 'Your have logged but your account currently not active. Please contact adminstrator for more details.';

            }

        }else{
            $status_info = 'Your must logged in to perform this action.';
        }


        return redirect('login')->with('status_info', $status_info);
    }
}
