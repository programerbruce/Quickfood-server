<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $status_info = '';

        if(Auth::check()){

            if(Auth::user()->isActive()) {

                if (Auth::user()->isAdmin()) {

                    return $next($request);

                } else {

                    $status_info = 'Your user status is active but you are not adminstrator at moment. Please contact us for more information';

                }

            }else{
                    $status_info = 'Your user status is not active at moment. Please contact us for more information';

                }

        }


        Auth::logout();


        return redirect('login')->with('status_info', $status_info);
    }
}
