<?php

namespace App\Http\Controllers;

use App\CategoryMenu;
use App\Menu;
use Illuminate\Http\Request;

class AdminMenuCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $menuCategories = CategoryMenu::all();


        return view('admin.menu.category.index', compact('menuCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.menu.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        CategoryMenu::create($request->all());

        return redirect(route('admin.menu.category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $category = CategoryMenu::findOrFail($id);

        return view('admin.menu.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        CategoryMenu::findOrFail($id)->update($request->all());

        return redirect(route('admin.menu.category.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $menus = Menu::where('category_menus_id', $id)->get();


        foreach ($menus as $menu) {

            if (file_exists(public_path() . $menu->photo)) {
                unlink(public_path() . $menu->photo);
            }

            $menu->delete();

        }

        CategoryMenu::findOrFail($id)->delete();

        return redirect(route('admin.menu.category.index'));


    }


}
