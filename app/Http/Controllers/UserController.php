<?php

namespace App\Http\Controllers;

use App\Order;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function updatePhone(Request $request){

        $id = Auth::user()->id;

        User::findOrFail($id)->update(['phone' => $request->phone]);

        return redirect(route('shop.user.index'))->with('status_info','Successfully updated your Phone!');
    }

    public function updateName(Request $request){

        $id = Auth::user()->id;

        User::findOrFail($id)->update(['name' => $request->name]);

        return redirect(route('shop.user.index'))->with('status_info','Successfully updated your Name!');

    }

    public function updatePhoto(Request $request){

        $user = Auth::user();

        if($user->photo != null){
            if (file_exists(public_path() . '/' . $user->photo->file)) {
                unlink(public_path() . '/' . $user->photo->file);
            }
        }




        if($file = $request->file('photo')){

            $name = $file->getClientOriginalName();

            $file->move('images/' . $user->id . '/',$name);



            $photo = Photo::create(['file'=>'images/' . $user->id . '/' . $name]);

            $user->photo_id = $photo->id;

            $user->save();

        }


        return redirect(route('shop.user.index'))->with('status_info','Successfully uploaded your Photo!');

    }

    public function orders($user_id){

        $orders = Order::where('user_id',$user_id)->paginate(6);

        foreach ($orders as $order) {
            $order->cart = unserialize($order->cart);
        }

        return view('shop.user.orders', compact('orders'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('shop.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
