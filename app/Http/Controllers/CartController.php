<?php

namespace App\Http\Controllers;

use App\lib\RoyalPayApi;
use App\lib\RoyalPayConfig;
use App\lib\RoyalPayExchangeRate;
use App\lib\RoyalPayRedirect;
use App\lib\RoyalPayUnifiedOrder;
use function App\lib\test;
use function App\lib\wp_print;
use App\Menu;

use App\Cart;

use App\Order;
use Carbon\Carbon;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Stripe\Charge;
use Stripe\Stripe;

class CartController extends Controller
{
    //
    public function addToCart(Request $request){

        $menu = Menu::findOrFail($request->menu_id);

        $choice = $request->choice;

        $qty= $request->qty;

        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $cart = new Cart($oldCart, $choice);

        $cart->add($menu,$menu->id, $qty);

        $request->session()->put('cart',$cart);

        return response()->json($cart);

//        return response()->json(['html'=> $html]);

    }

    public function minusFromCart(Request $request){

        $menu = Menu::findOrFail($request->menu_id);

        $choice = $request->choice;

        $qty= $request->qty;

        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $cart = new Cart($oldCart, $choice);

        $cart->minus($menu,$menu->id, $qty);

        if(count($cart->items) > 0){

            $request->session()->put('cart',$cart);
        }else{
            Session::forget('cart');
        }


        return response()->json($cart);

//        return response()->json(['html'=> $html]);

    }


    public function goToCheckout(Request $request){

        if(!Session::has('cart')){

            return view('shop.view.index').with('cart_empty','Please add at least a item to your cart before go to order page!');
        }

        if(!Session::has('cart')['addressDetail'] == null){

            return redirect('shop.cart.orderAddress').with('details_empty','Please fill out your details before enter into checkout!');
        }

        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $cart = new Cart($oldCart, $oldCart->choice);

        $validator = Validator::make($request->all(), [
            'firstname_order' => 'required',
            'lastname_order' => 'required',
            'tel_order' => 'required',
            'email_order' => 'required',
            'address_order' => 'required',
            'city_order' => 'required',
            'pcode_oder' => 'required',
            'timeOption' => 'required',
        ]);

        $delivery_schedule_time = $request->delivery_schedule_time;


        if ($validator->fails()) {
            return redirect(route('shop.cart.orderAddress'))
                ->withErrors($validator);
        }

        $now = Carbon::now('Australia/Melbourne');

        $timeOption = $request->timeOption;


        $desire_date = '';
        $delivery_schedule_time = str_replace(".",":",$delivery_schedule_time);


        if($timeOption == 1){

            $desire_date = $now;

        }else{
            $tommorow = $now->addDays(1);
            $desire_date = Carbon::createFromFormat(
                'd/m/Y g:ia',
                $tommorow->day . '/' . $tommorow->month . '/' . $tommorow->year . ' ' . $delivery_schedule_time,
                'Australia/Melbourne'
            );

        }

        $firstname = $request->firstname_order;
        $lastname = $request->lastname_order;
        $tel = $request->tel_order;
        $email = $request->email_order;
        $city = $request->city_order;
        $pcode = $request->pcode_oder;
        $address = $request->address_order . ',' . $city . ' ';
        $notes = $request->notes;


        $cart->setAddressDetail(
            $firstname,
            $lastname,$tel,
            $email,
            $address,
            $city,
            $pcode,
            $desire_date,
            $notes);

//        if($cart->choice == 1){
//            $cart->totalPrice += 10;
//        }

        $request->session()->put('cart',$cart);

        return view('shop.checkout.checkout');

    }


    public function checkout(Request $request){

        if(!Session::has('cart')){

            return view('shop.view.index').with('cart_empty','Please add at least a item to your cart before go to order page!');
        }

        if(!Session::has('cart')['addressDetail'] == null){

            return redirect('shop.cart.orderAddress').with('details_empty','Please fill out your details before enter into checkout!');
        }

        $oldCart = Session::get('cart');
//        return $oldCart->choice;
        $cart = new Cart($oldCart, $oldCart->choice);

        $order = new Order();

        Stripe::setApiKey('sk_test_n8KeOzpbq6JDiQ6tV6Mx7nBJ');
        try {
             $charge = Charge::create(array(
                'amount' => $cart->choice == 1 ? ($cart->totalPrice + 10) * 100 : $cart->totalPrice * 100,
                'currency' => 'usd',
                'description' => 'Test charge',
                'source' => $request->input('stripeToken'),
            ));

             $order->cart = serialize($cart);
             $order->amount = $charge->amount;
             $order->address = $cart->addressDetail['address'];
             $order->name = $cart->addressDetail['firstName'] . ' ' . $cart->addressDetail['lastName'];
             $order->desire_time = $cart->addressDetail['desireTime'];
             $order->status = 1;
             $order->payment_id = $charge->id;

             $orderNo = $order->payment_id;

             if(Auth::user()){
                 $order->user_id = Auth::user()->id;
             }


            $order->save();

        } catch (Exception $e) {
            return redirect()->route('shop.checkout.checkout').with('charge_error' , $e->getMessage());
        }

//        $orderInfo = '<CB>测试打印</CB><BR>';
//        $orderInfo .= '名称　　　　　 单价  数量 金额<BR>';
//        $orderInfo .= '--------------------------------<BR>';
//        $orderInfo .= '饭　　　　　 　10.0   10  10.0<BR>';
//        $orderInfo .= '炒饭　　　　　 10.0   10  10.0<BR>';
//        $orderInfo .= '蛋炒饭　　　　 10.0   100 100.0<BR>';
//        $orderInfo .= '鸡蛋炒饭　　　 100.0  100 100.0<BR>';
//        $orderInfo .= '西红柿炒饭　　 1000.0 1   100.0<BR>';
//        $orderInfo .= '西红柿蛋炒饭　 100.0  100 100.0<BR>';
//        $orderInfo .= '西红柿鸡蛋炒饭 15.0   1   15.0<BR>';
//        $orderInfo .= '备注：加辣<BR>';
//        $orderInfo .= '--------------------------------<BR>';
//        $orderInfo .= '合计：xx.0元<BR>';
//        $orderInfo .= '送货地点：广州市南沙区xx路xx号<BR>';
//        $orderInfo .= '联系电话：13888888888888<BR>';
//        $orderInfo .= '订餐时间：2014-08-08 08:08:08<BR>';
//        $orderInfo .= '<QR>http://www.dzist.com</QR>';//把二维码字符串用标签套上即可自动生成二维码

        $firstName = $cart->addressDetail['firstName'];
        $lastName = $cart->addressDetail['lastName'];
        $phone = $cart->addressDetail['phone'];
        $address = $cart->addressDetail['address'];
        $desireTime = $cart->addressDetail['desireTime'];
        $note = $cart->addressDetail['Note'];
        $totalPrice = $cart->totalPrice;
        $choice = $cart->choice;


//        $orderInfo = '<CB>Quick food</CB><BR>';
//        $orderInfo .= '名称　　　　　    数量<BR>';
//        $orderInfo .= '--------------------------------<BR>';

        $arr = array();

        foreach ($cart->items as $item){
            $name = $item['item']->name;
            $price = $item['item']->price;
            $qty = $item['qty'];

//            $item_subtotal = $item['price'];

//            $orderInfo .= $name . '　　　　　    ' . $qty . '<BR>';

            $orderItem= array('title'=>$name,'price'=>$price,'num'=>$qty);
            array_push($arr,$orderItem);
        }

        $orderInfo = test($arr,14,6,3,6,$orderNo,$address,$phone,$note,'',$desireTime,$choice);//名称14 单价6 数量3 金额6-->这里的字节数可按自己需求自由改写

//
//        $orderInfo .= '备注：' . $note . '<BR>';
//        $orderInfo .= '--------------------------------<BR>';
//        $orderInfo .= '合计：$' . $totalPrice . '<BR>';
//        $orderInfo .= '送货地点：' . $address . '<BR>';
//        $orderInfo .= '联系电话：' . $phone . '<BR>';
//        $orderInfo .= '订餐时间：2014-08-08 08:08:08<BR>';
//        $orderInfo .= '<QR>http://www.dzist.com</QR>';//把二维码字符串用标签套上即可自动生成二维码

        //提示：打印机编号(必填) # 打印机识别码(必填) # 备注名称(选填) # 流量卡号码(选填)，多台打印机请换行（\n）添加新打印机信息，每次最多100行(台)。
//        $snlist = "718500705#fjwww3vh#remark1#carnum1";
//        addprinter($snlist);


        wp_print('718500705',$orderInfo,1);

        Session::forget('cart');

        $data = [
            'title' => $order->name,
            'receipt_no' => $charge->id,
        ];

        Mail::send('email.email', $data, function ($message) use ($cart) {
            $message->to($cart->addressDetail['email'], $cart->addressDetail['firstName'])->subject('Thanks for place your order with us');
        });

        return redirect(route('shop.cart.receipt',$charge->id));


    }

    public function wechatCheckout(Request $request){
        $cart = Session::get('cart');
//        return $cart->totalPrice;
        $channel = 'Wechat';
        $input = new RoyalPayUnifiedOrder();
        $input->setOrderId(RoyalPayConfig::PARTNER_CODE . date("YmdHis"));
        $input->setDescription("test");
        $input->setChannel($channel);
        $input->setPrice(1);

//        $input->setPrice($cart->totalPrice * 100);
        $input->setCurrency("AUD");
        $input->setNotifyUrl("http://115.29.162.214/example/notify.php");
        $input->setOperator("123456");
        $currency = $input->getCurrency();

        if (!empty($currency) && $currency == 'CNY') {
            //建议缓存汇率,每天更新一次,遇节假日或其他无汇率更新情况,可取最近一个工作日的汇率
            $inputRate = new RoyalPayExchangeRate();
            $rate = RoyalPayApi::exchangeRate($inputRate);
            if ($rate['return_code'] == 'SUCCESS') {
                $real_pay_amt = $input->getPrice() / $rate['rate'] / 100;
                if ($real_pay_amt < 0.01) {
                    echo 'CNY amount shall be more than 0.01 AUD for current exchange rate';
                    exit();
                }
            }
        }
        $result = RoyalPayApi::qrOrder($input);
        $url2 = $result["code_url"];

        //跳转 redirect
        $inputObj = new RoyalPayRedirect();
        $inputObj->setRedirect(route('shop.cart.payCallBack',['orderId' => $input->getOrderId()]));

        return redirect(RoyalPayApi::getQRRedirectUrl($result['pay_url'], $inputObj));

    }

    public function aliCheckout(Request $request){
        $cart = Session::get('cart');
//        return $cart->totalPrice;
        $channel = 'Alipay';
        $input = new RoyalPayUnifiedOrder();
        $input->setOrderId(RoyalPayConfig::PARTNER_CODE . date("YmdHis"));
        $input->setDescription("test");
        $input->setChannel($channel);
        $input->setPrice(1);

//        $input->setPrice($cart->totalPrice * 100);
        $input->setCurrency("AUD");
        $input->setNotifyUrl("http://115.29.162.214/example/notify.php");
        $input->setOperator("123456");
        $currency = $input->getCurrency();

        if (!empty($currency) && $currency == 'CNY') {
            //建议缓存汇率,每天更新一次,遇节假日或其他无汇率更新情况,可取最近一个工作日的汇率
            $inputRate = new RoyalPayExchangeRate();
            $rate = RoyalPayApi::exchangeRate($inputRate);
            if ($rate['return_code'] == 'SUCCESS') {
                $real_pay_amt = $input->getPrice() / $rate['rate'] / 100;
                if ($real_pay_amt < 0.01) {
                    echo 'CNY amount shall be more than 0.01 AUD for current exchange rate';
                    exit();
                }
            }
        }
        $result = RoyalPayApi::qrOrder($input);
        $url2 = $result["code_url"];

        //跳转 redirect
        $inputObj = new RoyalPayRedirect();
        $inputObj->setRedirect(route('shop.cart.payCallBack',['orderId' => $input->getOrderId()]));

        return redirect(RoyalPayApi::getQRRedirectUrl($result['pay_url'], $inputObj));

    }

    public function payCallBack(Request $request){
        $orderId = $request->orderId;

        $cart = Session::get('cart');
        $order = new Order();

        $order->cart = serialize($cart);
        $order->amount =  $cart->choice == 1 ? $cart->totalPrice + 10: $cart->totalPrice;
        $order->address = $cart->addressDetail['address'];
        $desireTime = $cart->addressDetail['desireTime'];
        $order->name = $cart->addressDetail['firstName'] . ' ' . $cart->addressDetail['lastName'];
        $order->desire_time = $cart->addressDetail['desireTime'];
        $order->status = 1;
        $order->payment_id = $orderId;

        $orderNo = $order->payment_id;

        if(Auth::user()){
            $order->user_id = Auth::user()->id;
        }


        $order->save();

        $firstName = $cart->addressDetail['firstName'];
        $lastName = $cart->addressDetail['lastName'];
        $phone = $cart->addressDetail['phone'];
        $address = $cart->addressDetail['address'];
        $note = $cart->addressDetail['Note'];
        $totalPrice = $cart->totalPrice;
        $choice = $cart->choice;


//        $orderInfo = '<CB>Quick food</CB><BR>';
//        $orderInfo .= '名称　　　　　    数量<BR>';
//        $orderInfo .= '--------------------------------<BR>';

        $arr = array();

        foreach ($cart->items as $item){
            $name = $item['item']->name;
            $price = $item['item']->price;
            $qty = $item['qty'];

//            $item_subtotal = $item['price'];

//            $orderInfo .= $name . '　　　　　    ' . $qty . '<BR>';

            $orderItem= array('title'=>$name,'price'=>$price,'num'=>$qty);
            array_push($arr,$orderItem);
        }

        $orderInfo = test($arr,14,6,3,6,$orderNo,$address,$phone,$note,'',$desireTime,$choice);//名称14 单价6 数量3 金额6-->这里的字节数可按自己需求自由改写

//
//        $orderInfo .= '备注：' . $note . '<BR>';
//        $orderInfo .= '--------------------------------<BR>';
//        $orderInfo .= '合计：$' . $totalPrice . '<BR>';
//        $orderInfo .= '送货地点：' . $address . '<BR>';
//        $orderInfo .= '联系电话：' . $phone . '<BR>';
//        $orderInfo .= '订餐时间：2014-08-08 08:08:08<BR>';
//        $orderInfo .= '<QR>http://www.dzist.com</QR>';//把二维码字符串用标签套上即可自动生成二维码

        //提示：打印机编号(必填) # 打印机识别码(必填) # 备注名称(选填) # 流量卡号码(选填)，多台打印机请换行（\n）添加新打印机信息，每次最多100行(台)。
//        $snlist = "718500705#fjwww3vh#remark1#carnum1";
//        addprinter($snlist);


        wp_print('718500705',$orderInfo,1);

        Session::forget('cart');

        $data = [
            'title' => $order->name,
            'receipt_no' => $orderId,
        ];

        Mail::send('email.email', $data, function ($message) use ($cart) {
            $message->to($cart->addressDetail['email'], $cart->addressDetail['firstName'])->subject('Thanks for place your order with us');
        });


        return redirect(route('shop.cart.receipt',['receipt_no' => $orderId]));
    }



    public function receipt($receipt_no){

        $order = Order::where('payment_id',$receipt_no)->get()->first();

        $cart = unserialize($order->cart);

        return view('shop.checkout.receipt', compact('order', 'cart'));

    }


    public function updateChoice(Request $request){

        if(!Session::has('cart')){

            return view('shop.view.index').with('cart_empty','Please add at least a item to your cart before go to order page!');
        }

        $cart = Session::get('cart');

        $cart->setChoice($request->choice);

        $request->session()->put('cart',$cart);

        return redirect(route('shop.cart.orderAddress'));

    }


    public function orderAddress(){

        return view('shop.checkout.orderaddress');

    }




}
