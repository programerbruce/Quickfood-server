<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class AdminOrderController extends Controller
{
    //

    public function index()
    {
        //

//        $posts = Post::paginate(3);


        return view('admin.order.index');

    }

    public function ajaxRenderOrder(Request $request)
    {
        $orders = Order::where('status', 1)->orderBy('created_at', 'asc')->get();

        foreach ($orders as $order) {
            $order->cart = unserialize($order->cart);
        }

        return response()->json($orders);

    }

    public function ajaxUpdateOrderToKitchen(Request $request)
    {

        Order::findOrFail($request->order_id)->update(['status' => 2]);

        return redirect(route('admin.order.index'));
    }


    public function goToKitchen()
    {
        //

//        $posts = Post::paginate(3);


        return view('admin.order.kitchen');

    }

    public function ajaxRenderKitchenOrder(Request $request)
    {

        $orders = Order::where('status', 2)->orderBy('updated_at', 'asc')->get();
        foreach ($orders as $order) {
            $order->cart = unserialize($order->cart);
        }


        return response()->json($orders);
    }

    public function ajaxUpdateOrderToDelivery(Request $request)
    {

        Order::findOrFail($request->order_id)->update(['status' => 3]);

        return redirect(route('admin.order.kitchen'));
    }

    public function orderManagement(Request $request)
    {

        $orders = Order::paginate(6);

        foreach ($orders as $order) {
            $order->cart = unserialize($order->cart);
        }

        return view('admin.order.management', compact('orders'));
    }

    public function orderManagementUpdate(Request $request)
    {

        $order_selected_status = $request->status;

        if ($order_selected_status == '') {
            $order_selected_status = null;
        }


        Order::findOrFail($request->order_id)->update(['status' => $order_selected_status]);


        return redirect(route('admin.order.management'));

    }

    public function goToDelivery(){

        return view('admin.order.delivery');
    }

    public function ajaxRenderDeliveryOrder(Request $request){

        $orders = Order::where('status', 3)->orderBy('updated_at', 'asc')->get();
        foreach ($orders as $order) {
            $order->cart = unserialize($order->cart);
        }

        return response()->json($orders);
    }

    public function ajaxUpdateOrderToFinish(Request $request)
    {

        Order::findOrFail($request->order_id)->update(['status' => 4]);

        return redirect(route('admin.order.goToDelivery'));
    }

}
