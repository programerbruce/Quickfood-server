<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\AdminPostStoreRequest;
use App\Photo;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::all();

        return view('admin.blog.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::pluck('name','id')->all();

        return view('admin.blog.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminPostStoreRequest $request)
    {
        //

        $data = [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'category_id' => $request->category_id,
            'body' => $request->body,
        ];

        $photo = $request->photo;

        $user = Auth::user();

        if($file = $request->file('photo')){

            $name = $file->getClientOriginalName();

            $file->move('images/' . $user->id . '/blog/post/',$name);



            $photo = Photo::create(['file'=>'images/' . $user->id . '/blog/post/' . $name]);

            $data['photo_id'] = $photo->id;

        }

        Post::create($data);

        return redirect('/admin/blog');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $post = Post::findOrFail($id);


        return view('blog.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $post = Post::findOrFail($id);

        return view('admin.blog.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Post::findOrFail($id)->update($request->all());

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);

        if (file_exists(public_path() . '/' . $post->photos()->first()->file)) {
            unlink(public_path() . '/' . $post->photos()->first()->file);
        }

        $post->delete();

        return redirect(route('admin.blog.index'));

    }

    public function updatePhoto(Request $request){

        $post = Post::findOrfail($request->post_id);

        $data = $request->all();

        if($post->photos()->first() != null){
            if (file_exists(public_path() . '/' . $post->photos()->first()->file)) {
                unlink(public_path() . '/' . $post->photos()->first()->file);
            }
        }

        if($file = $request->file('photo')){

            $name = $file->getClientOriginalName();

            $file->move('images/blog/post/',$name);



            $photo = Photo::create(['file'=>'images/blog/post/' . $name]);

            $post->photo_id = $photo->id;

            $post->save();
        }

        return redirect(route('admin.blog.edit', $post->id));


    }


    public function addCategory(Request $request){

        Category::create(['name' => $request->new_category_name]);

        return redirect(route('admin.blog.create'));

    }
}
