<?php

namespace App\Http\Controllers;

use App\CategoryMenu;
use App\Http\Requests\AdminMenuStoreRequest;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $menus = Menu::all();

        return view('admin.menu.index', compact('menus'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menuCategories = CategoryMenu::pluck('name','id')->all();

        return view('admin.menu.create', compact('menuCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminMenuStoreRequest $request)
    {
        //
        $input = [

            'name'=>$request->name,
            'category_menus_id'=>$request->category_menus_id,
            'description'=>$request->description,
            'price'=>$request->price,

        ];


        if($file = $request->file('photo')){

            $name = time() . $file->getClientOriginalName();

            $file->move('images/menu/',$name);

            $input['photo'] = '/images/menu/' . $name;

        }

        Menu::create($input);

        return redirect(route('admin.menu.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $menu = Menu::findOrFail($id);

        $categories = CategoryMenu::pluck('name','id')->all();

        return view('admin.menu.edit', compact('menu', 'categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminMenuStoreRequest $request, $id)
    {
        //

        Menu::findOrFail($id)->update($request->all());

        return redirect(route('admin.menu.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $menu = Menu::findOrFail($id);

        if (file_exists(public_path() . $menu->photo)) {
            unlink(public_path() . $menu->photo);
        }

            $menu->delete();


        return redirect(route('admin.menu.index'));
    }


    public function photo(Request $request){

        $menu = Menu::findOrFail($request->menu_id);

        if($menu->photo) {
            if (file_exists(public_path() . $menu->photo)) {
                unlink(public_path() . $menu->photo);

                $menu->update(['photo' => null]);
            }
        }

        if($file = $request->file('photo')){

            $name = time() . $file->getClientOriginalName();

            $file->move('images/menu/',$name);

            $menu->update(['photo'=>'/images/menu/' . $name]);

        }

        return redirect(route('admin.menu.edit', $menu->id));
    }
}
