<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    protected $fillable = [
        'user_id',
        'name',
        'address',
        'amount',
        'cart',
        'desire_time',
        'expect_time',
        'payment_id',
        'status',
    ];
}
